import type { Settings as LayoutSettings } from '@ant-design/pro-layout';
import { PageLoading } from '@ant-design/pro-layout';
import type { RunTimeLayoutConfig } from 'umi';
import { history, Link, RequestConfig } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
// import { currentUser as queryCurrentUser } from './services/ant-design-pro/api';
import {  queryDicts, queryCurrentUser, getReceiveDirectors } from './services/v1/api';
import { BookOutlined, LinkOutlined } from '@ant-design/icons';
import type { DictType } from '@/types/project';
// import useAccess from '@/utils/useAccess';
// import { btnAuthList, menuAuthList } from '../config/access';
import paramFormat from '@/utils/paramFormat';
import {roleList, getAccess} from '@/pages/access/role/service';
import { version } from '../package.json';

const isDev = process.env.NODE_ENV === 'development';
const loginPath = '/user/login';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */
export async function getInitialState(): Promise<{
  settings?: Partial<LayoutSettings>;
  currentUser?: API.CurrentUser;
  // fetchUserInfo?: () => Promise<API.CurrentUser | undefined>;
  menuData: Record<string, any>[], // 菜单权限
  dicts: Record<DictType, any>|{}, // 项目字段
  menuAccessDicts: Record<DictType, any>|{}, // 全量菜单权限
  actionAccessDicts: Record<API.BtnAuthType, any>|{}, // 全量菜单
}> {
  // 获取全量权限
  const fetchAccess = async () => {
    try {
      const [menuRes, btnRes] = await Promise.all([getAccess('menu'), getAccess('btn')]);

      return [menuRes?.data || {}, btnRes?.data || {}];
    } catch (error) {
      history.push(loginPath);
    }
    return [{}, {}];
  }

  // 获取用户信息
  const fetchUserInfo = async (id: string) => {
    try {
      const msg = await queryCurrentUser({id});
      const accessList = [...(msg.data?.menuAuthList || []), ...(msg.data?.btnAuthList||[])].map(d => d?.key)
      const accessKeySet = new Set(accessList)

      return {
        ...(msg.data||{}),
        accessKeySet
      };
    } catch (error) {
      history.push(loginPath);
    }
    return undefined;
  };
  // 获取枚举信息
  const fetchDicts = async (types: DictType[]) => {
    let dictsRes: any[];
    try {
      dictsRes = await Promise.all(
       types.map(t => queryDicts(t))
      )
    } catch (err) {
      dictsRes = []
    }

    const res = {} as Record<DictType, any>;
    types.forEach((t, i) => {
      res[t] = {};
      (dictsRes[i]?.data||[]).forEach((item: Record<string, string>) => {
        if (item?.dictKey) {
          res[t][item.dictKey] = item.dictName
        }
      })
    })

     try {
      const r = await roleList();
      res.ROLES = {};
      (r?.data?.recordList||[]).forEach((item: any) => {
        if (item?.id) {
          res.ROLES[item.id + ''] = item.roleName
        }
      })
    } catch (err) {}

    return res as Record<DictType, any>;
  }
  // 获取負責人列表
  const fetchReceiveDirectors = async () => {
    let receiveDirectors: any[];
    try {
      const res = await getReceiveDirectors();
      receiveDirectors = res.data;
    } catch (err) {
      receiveDirectors = []
    }
    return receiveDirectors;
  }

  // 如果是登录页面，不执行
  if (!history.location.pathname.startsWith(loginPath)) {
    const userId = localStorage.getItem('user_id');
    if (!userId) {
      history.push(loginPath);
      return {
        // fetchUserInfo,
        settings: {},
        menuData: [],
        dicts: {},
        menuAccessDicts: [],
        actionAccessDicts: [],
      };
    }
    const currentUser = await fetchUserInfo(userId);
    // const menuData = await fetchMenus();
    const dicts = currentUser ? await fetchDicts(['BUSINESS_TYPE', 'HONOR_PAY_TYPE', 'NODE_TYPE', 'PAY_TYPE', 'PROJECT_STATUS', 'PROJECT_TYPE', 'MONTH']) : {};

    const [menuAuthList, btnAuthList] = await fetchAccess();
    const receiveDirectors = await fetchReceiveDirectors();

    return {
      fetchUserInfo,
      receiveDirectors,
      currentUser,
      settings: {},
      // menuData,
      menuAccessDicts: menuAuthList,
      actionAccessDicts: btnAuthList,
      dicts
    };
  }
  return {
    // fetchUserInfo,
    receiveDirectors: [],
    settings: {},
    menuData: [],
    dicts: {},
    menuAccessDicts: [],
    actionAccessDicts: [],
  };
}

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout: RunTimeLayoutConfig = ({ initialState }) => {
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    headerTitleRender: (logo, title: string) => {
      return <div className="header-title">{title}<span className="version">v{version}</span></div>
    },
    waterMarkProps: {
      // content: initialState?.currentUser?.name,
    },
    footerRender: () => <Footer />,
    onPageChange: () => {
      const { location } = history;
      // 如果没有登录，重定向到 login
      if (!initialState?.currentUser && location.pathname !== loginPath) {
        history.push(loginPath);
      }
    },
    links: isDev
      ? [
          <Link to="/umi/plugin/openapi" target="_blank">
            <LinkOutlined />
            <span>OpenAPI 文档</span>
          </Link>,
          <Link to="/~docs">
            <BookOutlined />
            <span>业务组件文档</span>
          </Link>,
        ]
      : [],
    menuHeaderRender: undefined,
    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    ...initialState?.settings,
  };
};

/* 配置request */
export const request: RequestConfig = {
  timeout: 10000,
  middlewares: [],
  requestInterceptors: [(url, options) => {
    return {
      url,
      options: paramFormat(options),
    }
  }],
  responseInterceptors: [
    // 403 认证失效
    async(response, options) => {
      try {
        const data = await response.clone().json();
        if (data?.code === 403) {
          history.push(loginPath);
        }
        return response;
      } catch(error) {
        return response;
      }

    },
    // 去除列表页面的total
    async(response, options) => {
      try {
        const res = await response.clone().json();
        return {
          ...res || {},
          total: res?.data?.totalCount || 0
        };
      } catch(error) {
        return response;
      }
    }
  ],
  middlewares: [
      async function middlewareA(ctx, next) {
        try {
          await next();
        } catch(err) {
          console.log(err, 'err')
          throw new Error('请求错误，请稍后再试!')
        }
      },
  ],
  errorConfig: {
    adaptor: (resData) => {
      return {
        ...resData,
        success: resData?.code == undefined || resData.code === 200,
        errorMessage: resData?.desc,
      };
    },
  },
  prefix: isDev ? '/devProxy' : '/api/v1'
  // prefix: isDev ? '/devProxy' : 'http://120.55.75.3:8080'
};
