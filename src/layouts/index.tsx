import React from 'react';
import moment from 'moment';
import { IRouteComponentProps, useModel } from 'umi';
import { Input, DatePicker, Tag, Select, Switch, InputNumber } from 'antd';
import ProProvider from '@ant-design/pro-provider';
import { isNil } from 'lodash';
// import useDict from '@/utils/useDict';

export default function Layout({ children, location, route, history, match }: IRouteComponentProps) {
  const values = React.useContext(ProProvider);
  const currentTime = moment();

  const {initialState} = useModel('@@initialState');
  const {menuAccessDicts = [], actionAccessDicts = [], currentUser} = initialState;
  const {accessKeySet = new Set()} = currentUser || {};
  // // 增加字典枚举
  // const dicts = useDict(['BUSINESS_TYPE', 'HONOR_PAY_TYPE', 'NODE_TYPE', 'PAY_TYPE', 'PROJECT_STATUS', 'PROJECT_TYPE']);

  return (
    <ProProvider.Provider
      value={{
        ...values,
        // xxx: 自定义valueType失效 在makeColumns中实现
        // valueTypeMap: {
        //   amount: {
        //     render: (text) => <span>{text}(万)</span>,
        //     renderFormItem: (text, props) => (
        //       <Input placeholder="请输入" {...props?.fieldProps} />
        //     ),
        //   },
        //   sameMonthDateTime: {
        //     render: (text) => {
        //       return (
        //         <span>{moment(text?.[0]).format('YYYY-MM-DD')} 到 {moment(text?.[1]).format('YYYY-MM-DD')}</span>
        //       )
        //     },
        //     renderFormItem: (text, props) => {
        //       const [selectedTime, setSelectedTime] = React.useState<string>();
        //       const onCalendarChange = (times: string[]) => {
        //         const [t1, t2] = times || [];
        //         setSelectedTime(t1 || t2);
        //       }

        //       const isDisabledDate = (date: any) => {
        //         if (!selectedTime) return false;
        //         let disabled;
        //         try {
        //           disabled = !moment(date).isSame(currentTime, 'month');
        //         } catch {
        //         }
        //         return disabled;
        //       }
        //       return <DatePicker.RangePicker {...props?.fieldProps} value={moment(props?.fieldProps?.value)} onCalendarChange={onCalendarChange} disabledDate={isDisabledDate} />
        //     },
        //   },
        //   tags: {
        //     render: (text) => {
        //       return (
        //         <div style={{maxHeight: 50,  overflow: 'auto'}}>
        //           {[text].flat(1).map((item) => (
        //             <Tag key={item.key} style={{marginBottom: 4}}>{item.name}</Tag>
        //           ))}
        //         </div>
        //       );
        //     },
        //     renderFormItem: (value, props) => {
        //       return (
        //         <Select
        //           {...props?.fieldProps}
        //           onChange={(v) => {
        //             console.log(v, 'sakjhdkkoj')
        //             props.fieldProps.onChange(v)
        //           }}
        //           // defaultValue={value}
        //           labelInValue
        //           // optionLabelProp="name"
        //           size="small"
        //           mode="multiple"
        //         />
        //       )
        //     },
        //   },
        //   switch: {
        //     render: (text) => {
        //       return (
        //         <span>{text ? '是': '否'}</span>
        //       );
        //     },
        //     renderFormItem: (text, props) => {
        //       return (
        //         <Switch
        //           defaultChecked={text}
        //           {...props?.fieldProps} /* value={value} */
        //           size="small"
        //         />
        //       )
        //     },
        //   },
        //   customPercent: {
        //     render: (text) => {
        //       if (isNil(text)) return <span>-</span>;
        //       return (
        //         <span>{(text||0)*100}%</span>
        //       );
        //     },
        //     renderFormItem: (text, props) => {
        //       return (
        //         <InputNumber
        //           max={1}
        //           min={0}
        //           {...props?.fieldProps} /* value={value} */
        //         />
        //       )
        //     },
        //   },
        // },
      }}
    >
      {React.cloneElement(children, {menuAccessDicts, actionAccessDicts, accessKeySet})}
    </ProProvider.Provider>
  )
}
