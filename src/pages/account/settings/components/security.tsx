import React from 'react';
import { useModel } from 'umi';
import { List, message, Modal } from 'antd';
import type { FormValueType } from './UpdateForm';
import { ProFormText, ModalForm } from '@ant-design/pro-form';
import { resetPassword } from '@/pages/access/user/service';

type Unpacked<T> = T extends (infer U)[] ? U : T;

/**
 * 更新密码
 * @param fields
 */
const handleUpdatePassword = async (id: string, pwd: string) => {
  console.log(id, pwd, 'asdlaskjd')
  try {
    const res = await resetPassword({
      id,
      password: pwd
    });

    if (res.code === 200) {
      message.success('修改成功！');
      return true;
    }
    return true;
  } catch (error) {
    return false;
  }
};

const SecurityView: React.FC = () => {
  const { initialState } = useModel('@@initialState');
  const { currentUser } = initialState || {};

  const getData = () => [
    {
      title: '账户密码',
      description: (
        <>
          ******
          {/* {passwordStrength.strong} */}
        </>
      ),
      actions: [
        <ModalForm
          trigger={<a key="Modify">修改</a>}
          title="修改密码"
          width={520}
          layout="horizontal"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 12 }}
          onFinish={async (value) => {
            const success = await handleUpdatePassword(currentUser?.id, value?.password);
          }}
        >
          <ProFormText.Password
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码' }]}
            fieldProps={{
              type: 'password'
            }}
            placeholder="请输入密码"
          />
        </ModalForm>
      ],
    },
    // {
    //   title: '密保手机',
    //   description: `已绑定手机：138****8293`,
    //   actions: [<a key="Modify">修改</a>],
    // },
    // {
    //   title: '密保问题',
    //   description: '未设置密保问题，密保问题可有效保护账户安全',
    //   actions: [<a key="Set">设置</a>],
    // },
    // {
    //   title: '备用邮箱',
    //   description: `已绑定邮箱：ant***sign.com`,
    //   actions: [<a key="Modify">修改</a>],
    // },
    // {
    //   title: 'MFA 设备',
    //   description: '未绑定 MFA 设备，绑定后，可以进行二次确认',
    //   actions: [<a key="bind">绑定</a>],
    // },
  ];

  const data = getData();
  return (
    <>
      <List<Unpacked<typeof data>>
        itemLayout="horizontal"
        dataSource={data}
        renderItem={(item) => (
          <List.Item actions={item.actions}>
            <List.Item.Meta title={item.title} description={item.description} />
          </List.Item>
        )}
      />
    </>
  );
};

export default SecurityView;
