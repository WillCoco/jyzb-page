import {
  LockOutlined,
  MobileOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Alert, message, Tabs, Card } from 'antd';
import cls from 'classnames';
import React, { useState } from 'react';
import { ProFormCaptcha, ProFormText, LoginForm } from '@ant-design/pro-form';
import { useIntl, history, FormattedMessage, useModel } from 'umi';
import Footer from '@/components/Footer';
import { login } from '@/services/ant-design-pro/api';
import {roleList, getAccess,} from '@/pages/access/role/service';
import { getFakeCaptcha } from '@/services/ant-design-pro/login';
import { queryDicts, getReceiveDirectors } from '@/services/v1/api';

import type { DictType } from '@/types/project';

import Wave from './components/wave';
import styles from './index.less';

const LoginMessage: React.FC<{
  content: string;
}> = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

const Login: React.FC = () => {
  const [userLoginState, setUserLoginState] = useState<API.LoginResult>({});
  const [type, setType] = useState<string>('account');
  const { initialState, setInitialState } = useModel('@@initialState');

  const intl = useIntl();
  // 获取全量权限
  const fetchAccess = async () => {
    try {
      const [menuRes, btnRes] = await Promise.all([getAccess('menu'), getAccess('btn')]);

      return [menuRes?.data || {}, btnRes?.data || {}];
    } catch (error) {
      return [{}, {}];
    }
    return [{}, {}];
  }

    // 获取枚举信息
  const fetchDicts = async (types: DictType[]) => {
    let dictsRes: any[];
    try {
      dictsRes = await Promise.all(
       types.map(t => queryDicts(t))
      )
    } catch (err) {
      dictsRes = []
    }

    const res = {} as Record<DictType, any>;
    types.forEach((t, i) => {
      res[t] = {};
      (dictsRes[i]?.data||[]).forEach((item: Record<string, string>) => {
        if (item?.dictKey) {
          res[t][item.dictKey] = item.dictName
        }
      })
    })

    try {
      const r = await roleList();
      res.ROLES = {};
      (r?.data?.recordList||[]).forEach((item: any) => {
        if (item?.id) {
          res.ROLES[item.id + ''] = item.roleName
        }
      })
    } catch (err) {
    }
    return res as Record<DictType, any>;
  }

    // 获取負責人列表
  const fetchReceiveDirectors = async () => {
    let receiveDirectors: any[];
    try {
      const res = await getReceiveDirectors();
      receiveDirectors = res.data;
    } catch (err) {
      receiveDirectors = []
    }

    return receiveDirectors;
  }

  const handleSubmit = async (values: API.LoginParams) => {
    try {
      // 登录
      const msg = await login({ ...values });
      if (msg.code === 200) {
        const defaultLoginSuccessMessage = intl.formatMessage({
          id: 'pages.login.success',
          defaultMessage: '登录成功',
        });

        message.success(defaultLoginSuccessMessage);

        // 权限
        const accessList = [...(msg.data?.menuAuthList || []), ...(msg.data?.btnAuthList||[])].map(d => d?.key) as API.BtnAuthType & Record<string, string>;
        const accessKeySet = new Set(accessList)

        localStorage.setItem('user_id', msg?.data?.id);

        // dict
        const dicts = await fetchDicts(['BUSINESS_TYPE', 'HONOR_PAY_TYPE', 'NODE_TYPE', 'PAY_TYPE', 'PROJECT_STATUS', 'PROJECT_TYPE', 'MONTH']);

        // 收款負責人
        const receiveDirectors = await fetchReceiveDirectors();

        const [menuAuthList, btnAuthList] = await fetchAccess();

        await setInitialState((s) => ({
          ...s,
          currentUser: {
            ...(msg?.data||{}),
            accessKeySet
          },
          dicts,
          menuAccessDicts: menuAuthList,
          actionAccessDicts: btnAuthList,
          receiveDirectors
        }) as any);
        // await fetchUserInfo();
        /** 此方法会跳转到 redirect 参数所在的位置 */
        if (!history) return;
        const { query } = history.location;
        const { redirect } = query as { redirect: string };
        history.push(redirect || '/');
        return;
      }
      console.log(msg);
      // 如果失败去设置用户错误信息
      setUserLoginState(msg);
    } catch (error) {
      console.error(error)
    }
  };
  const { status, type: loginType } = userLoginState;

  const waveRef = React.useRef<HTMLDivElement>(null);

  // 创建波浪
  React.useEffect(() => {
    if (document.body.clientWidth > 600) {
      const wave = new Wave(waveRef.current);
      wave.init();
    }

    return () => {
    }
  }, [])

  return (
    <div ref={waveRef} className={cls([styles.container, 'login-container'])}>
      {/* 不需要国际化 */}
      {/* <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang />}
      </div> */}
      <div className={styles.content}>
        <Card
          style={{width: 400, margin: '0 auto', borderRadius: 8}}
          bodyStyle={{paddingBottom: 46}}
        >
          <LoginForm
            className="login-form"
            // logo={<img alt="logo" src="/logo.svg" />}
            title={<h2 className={styles.title}>全链事业部经营性指标管理平台</h2>}
            subTitle={<p className={styles['sub-title']}>账号密码登录</p>}
            initialValues={{
              autoLogin: true,
            }}
            onFinish={async (values) => {
              await handleSubmit(values as API.LoginParams);
            }}
          >
            <Tabs className={styles.tabs} activeKey={type} onChange={setType}>
              <Tabs.TabPane
                key="account"
                tab={intl.formatMessage({
                  id: 'pages.login.accountLogin.tab',
                  defaultMessage: '账户密码登录',
                })}
              />
              <Tabs.TabPane
                key="mobile"
                tab={intl.formatMessage({
                  id: 'pages.login.phoneLogin.tab',
                  defaultMessage: '手机号登录',
                })}
              />
            </Tabs>

            {status === 'error' && loginType === 'account' && (
              <LoginMessage
                content={intl.formatMessage({
                  id: 'pages.login.accountLogin.errorMessage',
                  defaultMessage: '账户或密码错误(admin/ant.design)',
                })}
              />
            )}
            {type === 'account' && (
              <>
                <ProFormText
                  name="username"
                  fieldProps={{
                    size: 'large',
                    prefix: <UserOutlined className={styles.prefixIcon} />,
                  }}
                  placeholder={intl.formatMessage({
                    id: 'pages.login.username.placeholder',
                    defaultMessage: '用户名',
                  })}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="pages.login.username.required"
                          defaultMessage="请输入用户名!"
                        />
                      ),
                    },
                  ]}
                />
                <ProFormText.Password
                  name="password"
                  fieldProps={{
                    size: 'large',
                    prefix: <LockOutlined className={styles.prefixIcon} />,
                  }}
                  placeholder={intl.formatMessage({
                    id: 'pages.login.password.placeholder',
                    defaultMessage: '密码: ant.design',
                  })}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="pages.login.password.required"
                          defaultMessage="请输入密码！"
                        />
                      ),
                    },
                  ]}
                />
              </>
            )}

            {status === 'error' && loginType === 'mobile' && <LoginMessage content="验证码错误" />}
            {type === 'mobile' && (
              <>
                <ProFormText
                  fieldProps={{
                    size: 'large',
                    prefix: <MobileOutlined className={styles.prefixIcon} />,
                  }}
                  name="mobile"
                  placeholder={intl.formatMessage({
                    id: 'pages.login.phoneNumber.placeholder',
                    defaultMessage: '手机号',
                  })}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="pages.login.phoneNumber.required"
                          defaultMessage="请输入手机号！"
                        />
                      ),
                    },
                    {
                      pattern: /^1\d{10}$/,
                      message: (
                        <FormattedMessage
                          id="pages.login.phoneNumber.invalid"
                          defaultMessage="手机号格式错误！"
                        />
                      ),
                    },
                  ]}
                />
                <ProFormCaptcha
                  fieldProps={{
                    size: 'large',
                    prefix: <LockOutlined className={styles.prefixIcon} />,
                  }}
                  captchaProps={{
                    size: 'large',
                  }}
                  placeholder={intl.formatMessage({
                    id: 'pages.login.captcha.placeholder',
                    defaultMessage: '请输入验证码',
                  })}
                  captchaTextRender={(timing, count) => {
                    if (timing) {
                      return `${count} ${intl.formatMessage({
                        id: 'pages.getCaptchaSecondText',
                        defaultMessage: '获取验证码',
                      })}`;
                    }
                    return intl.formatMessage({
                      id: 'pages.login.phoneLogin.getVerificationCode',
                      defaultMessage: '获取验证码',
                    });
                  }}
                  name="captcha"
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="pages.login.captcha.required"
                          defaultMessage="请输入验证码！"
                        />
                      ),
                    },
                  ]}
                  onGetCaptcha={async (phone) => {
                    const result = await getFakeCaptcha({
                      phone,
                    });
                    if (result === false) {
                      return;
                    }
                    message.success('获取验证码成功！验证码为：1234');
                  }}
                />
              </>
            )}
            {/* <div
              style={{
                marginBottom: 24,
              }}
            >
              <ProFormCheckbox noStyle name="autoLogin">
                <FormattedMessage id="pages.login.rememberMe" defaultMessage="自动登录" />
              </ProFormCheckbox>
              <a
                style={{
                  float: 'right',
                }}
              >
                <FormattedMessage id="pages.login.forgotPassword" defaultMessage="忘记密码" />
              </a>
            </div> */}
          </LoginForm>
        </Card>
      </div>
      <Footer />
    </div>
  );
};

export default Login;
