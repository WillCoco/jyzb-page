import type { Request, Response } from 'express';

/* 收款列表 */
const receiveRecord = (req: Request, res: Response, u: string) => {
  const result = {
	"code": 200,
	"data": {
		"recordList": [
			{
				"businessType": "tinei",
				"contractAmount": 21,
				"isBadDebt": true,
				"lastFyExpectReceivedAmount": 21,
				"lastFyTotalAmount": 21,
				"nextWeekEndDate": Date.now(),
				"nextWeekNodeCondition": "nextWeekNodeCondition",
				"nextWeekRate": "12",
				"nextWeekReceivableAmount": 21,
				"partnerName": "partnerName",
				"projectId": 21,
				"projectName": "projectName",
				"projectStatus": "projectStatus",
				"projectType": "tinei",
				"receivableAmount": 21,
				"receiveDirector": "receiveDirector",
				"receivedAmount": 21,
				"remark": "remark",
				"taxConfirmAmount": 21,
				"taxPoint": "12",
				"thisFyQ1Amount": 21,
				"thisFyQ2Amount": 21,
				"thisFyQ3Amount": 21,
				"thisFyQ4Amount": 21,
				"thisFySurplusQExpectReceivedAmount": 21,
				"thisFyTotalAmount": 10
			},
			{
				"businessType": "tinei",
				"contractAmount": 21,
				"isBadDebt": true,
				"lastFyExpectReceivedAmount": 21,
				"lastFyTotalAmount": 21,
				"nextWeekEndDate": Date.now(),
				"nextWeekNodeCondition": "nextWeekNodeCondition",
				"nextWeekRate": "12",
				"nextWeekReceivableAmount": 21,
				"partnerName": "partnerName",
				"projectId": 212,
				"projectName": "projectName",
				"projectStatus": "projectStatus",
				"projectType": "tinei",
				"receivableAmount": 21,
				"receiveDirector": "receiveDirector",
				"receivedAmount": 21,
				"remark": "remark",
				"taxConfirmAmount": 21,
				"taxPoint": "12",
				"thisFyQ1Amount": 21,
				"thisFyQ2Amount": 21,
				"thisFyQ3Amount": 21,
				"thisFyQ4Amount": 21,
				"thisFySurplusQExpectReceivedAmount": 21,
				"thisFyTotalAmount": 10
			},
		],
		"totalCount": 21,
		"totalPageNum": 0
	},
	"desc": ""
}
  return res.json(result)
};

/* 开票列表 */
const getTax = (req: Request, res: Response, u: string) => {
  const result = {
	"code": 200,
	"data": [
		{
			"createTime": Date.now(),
			"hasEffect": true,
			"id": 10,
			"projectId": 120,
			"receiveNodeId": 10,
			"taxAmount": 10,
			"taxDate": Date.now(),
			"taxNo": "1",
			"taxRate": 10,
			"updateTime": Date.now(),
			"userId": 10
		}
	],
	"desc": ""
}
  return res.json(result)
};

/* 收款列表 */
const receiveNode = (req: Request, res: Response, u: string) => {
  const result = {
	"code": 200,
	"data": [
		{
			"closeAmount": 11110,
			"completionRate": "2",
			"createTime": Date.now(),
			"endDate":  Date.now(),
			"id": 10,
			"nodeCondition": "nodeCondition",
			"nodeName": "nodeName",
			"projectId": 20,
			"receivableAmount": 220,
			"receivedAmount": 20,
			"type": "type",
			"unReceivedAmount": 20,
			"updateTime":  Date.now(),
			"userName": "userName"
		},
		{
			"closeAmount": 11110,
			"completionRate": "2",
			"createTime": Date.now(),
			"endDate":  Date.now(),
			"id": 11,
			"nodeCondition": "nodeCondition",
			"nodeName": "nodeName",
			"projectId": 20,
			"receivableAmount": 220,
			"receivedAmount": 20,
			"type": "type",
			"unReceivedAmount": 20,
			"updateTime":  Date.now(),
			"userName": "userName"
		},
	],
	"desc": ""
}
  return res.json(result)
};


/* 获取一年的现金流 */
const getYearTotal = (req: Request, res: Response) => {
  const result = {
    "code": 200,
    "data": {
      "contractAmount": 10,
      "lastFyExpectReceivedAmount": 10,
      "lastFyTotalAmount": 0,
      "nextWeekEndDate": "",
      "nextWeekNodeCondition": "",
      "nextWeekRate": "",
      "nextWeekReceivableAmount": 0,
      "receivableAmount": 10,
      "receivedAmount": 10,
      "taxConfirmAmount": 0,
      "thisFyQ1Amount": 10,
      "thisFyQ2Amount": 10,
      "thisFyQ3Amount": 10,
      "thisFyQ4Amount": 10,
      "thisFySurplusQExpectReceivedAmount": 110,
      "thisFyTotalAmount": 10
    },
    "desc": ""
  }
  return res.json(result)
};

export default {
  'GET /project/receivableAmount/page': receiveRecord,
  'GET /tax': getTax,
  'GET /receiveNode': receiveNode,
  'GET /project/receivable/total': getYearTotal,
};
