import { Button, message, Card, Select, DatePicker, Popover } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import withAccess from '@/utils/withAccess';
import makeColumns, { amountValueType, customPercentValueType } from '@/utils/columns/makeColumns';
import type { FormValueType } from './components/UpdateForm';
import {
  getReceiveRecord, getReceivableTotal, getTax,
  editTax, addTax, delTax,
  getNodes,
  getRecords, addRecord, delRecord, editRecord
} from './service';
import moment from 'moment';
import TotalCard from './components/TotalCard';
import { getExportExcel } from '@/services/v1/api';
import useProTable from '@/utils/useProTable';
import download from '@/utils/download';
import PopoverIcon from '@/components/PopoverIcon';

const AddBtn = withAccess('confirmIncome_add', 'a');
const EditBtn = withAccess('confirmIncome_edit', 'a');

const AddBtn2 = withAccess('confirmIncome_add', 'a');
const EditBtn2 = withAccess('confirmIncome_edit', 'a');

const ExportBtn = Button;

/**
 * 添加开票
 * @param fields
 */
const handleAddTax = async (fields: any) => {
  try {
    await addTax({ ...fields });
    message.success('添加成功');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 * 更新开票
 * @param fields
 */
const handleUpdateTax = async (fields: FormValueType, currentRow?: TableListItem) => {
  try {
    await editTax({
      ...currentRow,
      ...fields,
    });
    message.success('修改成功');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 * 删除开票
 * @param selectedRows
 */
const handleRemoveTax = async (id: string) => {
  if (!id) return true;
  try {
    await delTax(id);
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 * 增加记录
 * @param selectedRows
 */
const handleAddRecord = async (fields: any) => {
  if (!fields) return true;
  try {
    await addRecord(fields);
    message.success('添加成功');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 * 编辑记录
 * @param selectedRows
 */
const handleEditRecord = async (fields: any) => {
  if (!fields) return true;
  try {
    await editRecord(fields);
    message.success('修改成功');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 * 删除记录
 * @param selectedRows
 */
const handleRemoveRecord = async (id: string) => {
  if (!id) return true;
  try {
    await delRecord(id);
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};

const ReceivablesList: React.FC = () => {
  const mainActionRef = useRef<ActionType>();
  const actionRef = useRef<Record<string, ActionType>>({});
  const level2Ref = useRef<Record<string, ActionType>>({});

  const currentMoment = moment();

  // 年份
  const [year, setYear] = useState<string>(currentMoment.format('YYYY'));

  // 2级编辑row
  const [editableKeysL1, setEditableRowKeysL1] = useState<string[]>([]);
  // 3级编辑row
  const [editableKeysL2, setEditableRowKeysL2] = useState<string[]>([]);

  // 年合计数据
  const [totalData, setTotalData] = useState<Record<string, any>>();

  // 所有项目的节点map, 分项目存储
  const [allNodeMap, setAllNodeMap] = React.useState<any>({});

  // console.log(allNodeMap, 'allNodeMap')

  const getData = async (...p: any[]) => {
    getReceivableTotal({
      year
    })
      .then(r => {
        if (r?.code === 200) {
          setTotalData(r.data);
        }
      })
      .catch(console.error);
    return await getReceiveRecord(...p);
  }

  // 获取节点列表，生产节点map
  const getProjectNodes = async (projectId: string) => {
    const nodesRes = await getNodes({projectId});

    const map = {};
    (nodesRes.data||[]).forEach((item: any) => {
      if (item?.id) {
        map[item.id + ''] = item.nodeName;
      }
    })

    setAllNodeMap((allNodesMap: any) => {
      return {
        ...allNodesMap,
        [projectId]: map
      };
    });
    return nodesRes
  }

  // 一级展开项
  const [expandedLevel1, setExpandedLevel1] = React.useState<string[]>([]);

  // 二级展开项
  const [expandedLevel2, setExpandedLevel2] = React.useState<string[]>([]);

  const columnsMainBase: ProColumns<any>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      fixed: 'left',
    },
    {
      title: '业务类型',
      dataIndex: 'businessType',
    },
    {
      title: '项目状态',
      dataIndex: 'projectStatus',
    },
    {
      title: '项目类型',
      dataIndex: 'projectType',
      hideInForm: true,
    },
    {
      title: '单位名称',
      dataIndex: 'partnerName',
    },
    {
      title: '合同金额',
      dataIndex: 'contractAmount',
    },
    {
      title: '确认收入（含税）',
      dataIndex: 'taxConfirmAmount',
      width: 150
    },
    {
      title: '已到款',
      dataIndex: 'receivedAmount',
    },
    {
      title: '应收金额',
      dataIndex: 'receivableAmount',
    },
    {
      title: '实际可收金额',
      dataIndex: 'nodeCloseAmount',
      valueType: 'amount',
      // width: 120
    },
    {
      title: '下一周节点',
      dataIndex: 'none',
      sorter: false,
      children: [
        {
          title: '节点进度',
          dataIndex: 'nextWeekRate',
          valueType: 'customPercent',
          render: customPercentValueType.render('nextWeekRate'),
          renderFormItem: customPercentValueType.renderFormItem,
          // width: 120
        },{
          title: '收款条件',
          dataIndex: 'nextWeekNodeCondition',
          // width: 120
        },{
          title: '计划日期',
          dataIndex: 'nextWeekEndDate',
          valueType: 'date',
          // width: 120
        },{
          title: '收款金额',
          dataIndex: 'nextWeekReceivableAmount',
          valueType: 'amount',
          // width: 120
        },
      ]
    },
    {
      title: '本年度收款节点',
      sorter: false,
      dataIndex: 'receivableAmount',
        children: [
          {
            title: 'Q1',
            dataIndex: 'thisFyQ1Amount',
            valueType: 'amount',
            render: amountValueType.render,
            renderFormItem: amountValueType.renderFormItem
          },{
            title: 'Q2',
            dataIndex: 'thisFyQ2Amount',
            valueType: 'amount',
            render: amountValueType.render,
            renderFormItem: amountValueType.renderFormItem
          },{
            title: 'Q3',
            dataIndex: 'thisFyQ3Amount',
            valueType: 'amount',
            render: amountValueType.render,
            renderFormItem: amountValueType.renderFormItem
          },{
            title: 'Q4',
            dataIndex: 'thisFyQ4Amount',
            valueType: 'amount',
            render: amountValueType.render,
            renderFormItem: amountValueType.renderFormItem
          },
        ]
    },
    {
      title: '本年合计',
      dataIndex: 'thisFyTotalAmount',
      sorter: false
    },
    {
      title: '次年收款金额',
      dataIndex: 'lastFyTotalAmount',
      sorter: false
    },
    {
      title: '收款负责人',
      dataIndex: 'receiveDirector',
      sorter: false
    },
    {
      title: '坏账标记',
      dataIndex: 'isBadDebt',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      // dataIndex: 'name',
      valueType: 'option',
      width: 60,
      fixed: 'right',
      render: (_, record) => [
        <PopoverIcon
          key="add"
          accessKey="confirmIncome_add"
          popoverProps={{
            content: "新增开票信息",
          }}
          onClick={() => {
            setExpandedLevel1([record?.id]); // 设置展开
            setTimeout(() => {
              actionRef.current?.[record?.id]?.addEditRecord({
                id: `add_${Date.now()}`,
                projectId: record?.id
              }); // 设置展开
            })
          }}
        />,
        // <AddBtn
        //   key="add"
        //   style={{marginRight: 8}}
        //   onClick={() => {
        //     setExpandedLevel1([record?.id]); // 设置展开
        //     setTimeout(() => {
        //       actionRef.current?.[record?.id]?.addEditRecord({
        //         id: `add_${Date.now()}`,
        //         projectId: record?.id
        //       }); // 设置展开
        //     })
        //   }}
        // >
        //   新增开票信息
        // </AddBtn>,
      ],
    },
  ];

  const columnsMain = makeColumns(columnsMainBase, {
    search: ['projectName', 'projectType', 'businessType', 'projectStatus'],
    // hideInTable: ['businessType']
  })

  const columnsNodesBase = [
    {
      title: '收款日期',
      dataIndex: 'receiveDate',
    },
    // {
    //   title: '收款节点',
    //   dataIndex: 'receiveNodeId',
    //   valueType: 'select',
    //   valueEnum: currentNodeMap
    // },
    {
      title: '到账金额',
      dataIndex: 'amount',
    },
    {
      title: '支付方式',
      dataIndex: 'payType',
    },
    {
      title: '承兑到期时间',
      dataIndex: 'honorEndDate',
    },
    {
      title: '承兑类型',
      dataIndex: 'honorPayType',
    },
    {
      title: '承兑票号',
      dataIndex: 'honorTaxNo',
    },
    {
      title: '贴现税点',
      dataIndex: 'honorTaxRate',
      valueType: 'customPercent',
    },
    {
      title: '操作',
      dataIndex: 'name',
      valueType: 'option',
      width: 60,
      fixed: 'right',
      render: (_, record) => [
        <EditBtn
          key="edit"
          style={{marginRight: 8}}
          onClick={() => {
            setEditableRowKeysL2([record.id])
          }}
        >
          修改
        </EditBtn>,
      ],
    },
  ];

  const columnsNodes = makeColumns(columnsNodesBase);

  // 三级级详情展开 节点
  const expandableTableLevel2 = (record: any) => {
    return (
      <EditableProTable<any, any>
        bordered
        headerTitle="开票表格"
        actionRef={(ref) => level2Ref.current[record?.id] = ref}
        rowKey="id"
        search={false}
        toolBarRender={false}
        request={getRecords}
        params={{taxId: record?.id}}
        columns={columnsNodes}
        scroll={{x: 'max-content'}}
        editable={{
          type: 'single',
          editableKeys: editableKeysL2,
          onSave: async (rowKey: string, data) => {
            const { id: recordId, projectId } = record || {};
            const isAdd = (`${rowKey||''}`).startsWith('add_');
            if (isAdd) {
              // 新增记录
              await handleAddRecord({
                ...data,
                taxId: recordId,
                id: undefined,
                index: undefined,
              });
              level2Ref.current?.[recordId]?.reload();
              actionRef.current?.[projectId]?.reload();
              mainActionRef.current?.reload();
              return;
            }
           // 编辑记录
            await handleEditRecord({
              ...data,
              taxId: record?.id,
              index: undefined
            });
            level2Ref.current?.[recordId]?.reload();
            actionRef.current?.[record?.projectId]?.reload();
            mainActionRef.current?.reload();
          },
          onDelete: async (key: string) => {
            const { id: recordId, projectId } = record || {};
            await handleRemoveRecord(key);
            level2Ref.current?.[recordId]?.reload();
            actionRef.current?.[projectId]?.reload();
            mainActionRef.current?.reload();
          },
          onChange: setEditableRowKeysL2,
        }}
        recordCreatorProps={false && {
          record: {
            id: `add_${Date.now()}`,
          },
          // 设置按钮文案
          creatorButtonText: '新增开票',
          style: {display: 'none'}
        }}
        pagination={false}
        style={{padding: 4}}
      />
    )
  }

  const columnsDetailBase = [
    {
      title: '开票日期',
      dataIndex: 'taxDate',
      sorter: false,
    },
    {
      title: '收款节点',
      dataIndex: 'receiveNodeId',
      valueType: 'select',
      valueEnum: allNodeMap,
      sorter: false,
      render: (_, record) => {
        return allNodeMap[record?.projectId]?.[record?.receiveNodeId];
      },
      renderFormItem: (v, props) => {
        return (
          <Select
            {...props?.fieldProps}
            options={Object.entries(allNodeMap?.[v?.entry?.projectId] || {}).map(([value, label]) => ({value, label}))}
          />
        )
      },
    },
    {
      title: '发票号码',
      dataIndex: 'taxNo',
      sorter: false,
    },
    // {
    //   title: '是否生效',
    //   dataIndex: 'hasEffect',
    //   sorter: false,
    //   formItemProps: {
    //     valuePropName: 'checked',
    //     initialValue: false,
    //   }
    // },
    {
      title: '收款金额',
      dataIndex: 'taxAmount',
      sorter: false,
    },
    {
      title: '税点',
      // dataIndex: 'taxRate',
      dataIndex: 'taxRate',
      valueType: 'customPercent',
      fieldProps: {
        max: 1,
        min: 0,
        placeholder: '请输入小数',
        step: 0.01
      },
      sorter: false,
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      editable: false,
      sorter: false,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      editable: false,
      sorter: false,
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      editable: false,
      sorter: false,
    },
    {
      title: '操作',
      // dataIndex: 'name',
      valueType: 'option',
      width: 120,
      fixed: 'right',
      sorter: false,
      render: (_, record) => [
        <AddBtn2
          key="add"
          style={{marginRight: 8}}
          onClick={() => {
            setExpandedLevel2([record?.id]); // 设置展开
            setTimeout(() => {
              level2Ref.current?.[record?.id]?.addEditRecord({
                id: `add_node_${Date.now()}`,
              }); // 设置展开
            })
          }}
        >
          <Popover content="新增收款记录">
            新增
          </Popover>
          {/* 新增收款记录 */}
        </AddBtn2>,
        <EditBtn2
          key="edit"
          style={{marginRight: 8}}
          onClick={() => {
            setEditableRowKeysL1([record.id])
          }}
        >
          修改
        </EditBtn2>,
      ],
    },
  ];

  const columnsDetail = makeColumns(columnsDetailBase, {
    required: ['taxAmount', 'taxDate', 'taxNo', 'taxRate']
  });

  // console.log(columnsDetail, 'columnsDetail')

  // 二级详情展开 开票信息
  const expandableTableLevel1 = (record: any) => {
    return (
      <EditableProTable<any, any>
        bordered
        headerTitle="开票表格"
        actionRef={(ref) => actionRef.current[record?.id] = ref}
        rowKey="id"
        search={false}
        toolBarRender={false}
        request={getTax}
        params={{projectId: record?.id}}
        postData={(v: any) => {
          console.log((v || []).map((d: any) => ({...d||{}, receiveNodeId: `${d?.receiveNodeId||''}`})), 22222)
          return (v || []).map((d: any) => ({...d||{}, receiveNodeId: `${d?.receiveNodeId||''}`}));
        }}
        columns={columnsDetail}
        scroll={{x: 'max-content'}}
        editable={{
          type: 'single',
          editableKeys: editableKeysL1,
          onSave: async (rowKey, data) => {
            const isAdd = (`${rowKey||''}`).startsWith('add_');
            if (isAdd) {
              const success = await handleAddTax({
                ...data,
                projectId: record?.id,
                id: undefined,
                index: undefined,
              });
              actionRef.current?.[record?.id]?.reload();
              mainActionRef.current?.reload();
              return;
            }
            // 新增和保存开票
            const success = await handleUpdateTax({
                ...data,
                index: undefined,
              });
            actionRef.current?.[record?.id].reload();
            mainActionRef.current?.reload();
          },
          onDelete: async (key: string) => {
            const success = await handleRemoveTax(key);
            actionRef.current?.[record?.id].reload();
            mainActionRef.current?.reload();
          },
          onChange: setEditableRowKeysL1,
        }}
        recordCreatorProps={{
          record: {
            id: `add_${Date.now()}`,
            hasEffect: true
          },
          // 设置按钮文案
          creatorButtonText: '新增开票',
          style: {
            display: 'none'
          }
        }}
        expandable={{
          columnWidth: 50,
          expandedRowRender : expandableTableLevel2,
          expandedRowKeys: expandedLevel2,
          onExpand: (v, record) => {
            setExpandedLevel2((state: any) => {
              const index = state.indexOf(record?.id);
              if (index >= 0) {
                state.splice(index, 1);
                return [...state];
              }
              return [...state, record?.id];
            })
          }
        }}
        pagination={false}
      />
    )
  }

  React.useEffect(() => {
    if (!expandedLevel1 || !expandedLevel1.length) {
      // setCurrentNodeMap({});
      return;
    };
    getProjectNodes(expandedLevel1[expandedLevel1.length - 1])
  }, [expandedLevel1]);

  const {
    onChange,
    orderByType,
    sortField,
    ref
  } = useProTable();

  return (
    <PageContainer
      extra={[
        <DatePicker
          key="picker"
          picker="year"
          defaultValue={moment()}
          onChange={(m, s) => setYear(s)}
        />
      ]}
    >
      <TotalCard data={totalData} year={year} />
      <Card style={{marginTop: 16}}>
        <ProTable<any, any>
          bordered
          headerTitle="查询表格"
          actionRef={mainActionRef}
          rowKey="id"
          onChange={onChange}
          formRef={ref}
          search={{
            span: 4,
            collapsed: false,
            optionRender: (searchConfig, formProps, dom) => {
              return [...dom, (
                <ExportBtn
                  key="export_button"
                  type="primary"
                  onClick={() => {
                    const fields = ref.current?.getFieldsValue() || {};
                    getExportExcel(`/project/receivable/export`, {
                      year,
                      ...fields,
                      orderByType,
                      sortField
                    })
                      .then(download)
                  }}>
                  导出
                </ExportBtn>
              )]
            }
          }}
          toolBarRender={false}
          request={getData}
          params={{
            year
          }}
          postData={(data) => {
            return data?.recordList || [];
          }}
          columns={columnsMain}
          expandable={{
            rowExpandable: record => record.expandable !== false,
            columnWidth: 50,
            expandedRowKeys: expandedLevel1,
            expandedRowRender : expandableTableLevel1,
            onExpand: (v, record) => {
              setExpandedLevel1((state: any) => {
                const index = state.indexOf(record?.id);
                if (index >= 0) {
                  state.splice(index, 1);
                  return [...state];
                }
                return [...state, record?.id];
              })
            }
          }}
          // summary={Summary}
          scroll={{x: 'max-content', y: 310}}
          style={{padding: 4}}
          pagination={{
          }}
        />
      </Card>
    </PageContainer>
  );
};

export default ReceivablesList;
