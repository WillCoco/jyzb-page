// @ts-ignore
/* eslint-disable */
import { request } from 'umi';


/** 获取应收列表 */
export async function getReceiveRecord(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/project/receivableAmount/page', {
    method: 'GET',
    params: {
      ...params,
    },
    otherParams: options
  });
}

/** 获取开票列表 */
export async function getTax(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/tax', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 添加开票 */
export async function addTax(
  data: any,
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/tax', {
    method: 'POST',
    data,
  });
}

/** 修改开票 */
export async function editTax(
  data: any,
) {
  return request<{
    data: any[];
    success?: boolean;
  }>('/tax', {
    method: 'PUT',
    data,
  });
}

/** 删除开票 */
export async function delTax(
  id: any,
) {
  return request<{
    data: any[];
    success?: boolean;
  }>(`/tax/${id}`, {
    method: 'DELETE',

  });
}

/** 获取收款节点 */
export async function getNodes(
  params: Record<string, any>,
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/receiveNode', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 应收账款表头合计 */
export async function getReceivableTotal(
  params: {
    year?: number|string;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/project/receivable/total', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 收款记录列表 */
export async function getRecords(
  params: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/receiveRecord', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增收款记录 */
export async function addRecord(
  data: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/receiveRecord', {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

/** 修改收款记录 */
export async function editRecord(
  data: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/receiveRecord', {
    method: 'PUT',
    data,
    ...(options || {}),
  });
}

/** 删除收款记录 */
export async function delRecord(
  id: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>(`/receiveRecord/${id}`, {
    method: 'DELETE',
  });
}
