import { Collapse, Space } from 'antd';
import { StatisticCard } from '@ant-design/pro-card';
const { Divider } = StatisticCard;

const valueStyle = {
  fontSize: 18
}

const bodyStyle = {
  padding: 8
}

const cardProps = {
  bodyStyle
}

function TotalCard(props: {
  data: any,
  year: any,
}) {
  const responsive = false;

  const {
    data,
    year
  } = props;

  const safeData = data || {};

  const amountTotal = (
    safeData.thisFyQ1Amount||0 +
    safeData.thisFyQ2Amount||0 +
    safeData.thisFyQ3Amount||0 +
    safeData.thisFyQAmount||0
  ).toFixed(2);

  return (
     <Collapse /* collapsible="header"  */defaultActiveKey={['1']}>
      <Collapse.Panel header={`合计`} key="1">
        <StatisticCard.Group direction={responsive ? 'column' : 'row'} style={{padding: 0}}>
        <StatisticCard
          statistic={{
            title: '合同金额',
            value: safeData.contractAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <StatisticCard
          statistic={{
            title: '确认收入（含税）',
            value: safeData.taxConfirmAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <StatisticCard
          statistic={{
            title: '实际收款金额',
            value: safeData.receivedAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <StatisticCard
          statistic={{
            title: '应收金额',
            value: safeData.receivableAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <StatisticCard
          statistic={{
            title: '实际可收金额',
            value: safeData.nodeCloseAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <StatisticCard
          statistic={{
            title: '本年度收款节点',
            tip: `Q1: ${safeData.thisFyQ1Amount}; Q2: ${safeData.thisFyQ2Amount}; Q3: ${safeData.thisFyQ3Amount}; Q: ${safeData.thisFyQ4Amount};`,
            value: amountTotal,
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        {/* <StatisticCard
          statistic={{
            title: '本年剩余季度',
            tip: `预计收款`,
            value: safeData.thisFySurplusQExpectReceivedAmount || 0,
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        /> */}
        {/* <Divider type={responsive ? 'horizontal' : 'vertical'} /> */}
        {/* <StatisticCard
          statistic={{
            title: '本年合计',
            value: safeData.thisFyTotalAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
        <Divider type={responsive ? 'horizontal' : 'vertical'} /> */}
        <StatisticCard
          statistic={{
            title: '次年总计',
            tip: '预计收款',
            value: safeData.lastFyTotalAmount || '0',
            precision: 2,
            suffix: '万',
            valueStyle
          }}
          {...cardProps}
        />
      </StatisticCard.Group>
      </Collapse.Panel>
    </Collapse>
  );
}

export default TotalCard;
