// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';

/* 获取计划列表 */
const receivePlan = (req: Request, res: Response, u: string) => {

  const result = {
    "code": 200,
    "data": {
      "currentPage": 0,
      "pageSize": 0,
      "recordList": [
        {
          "businessType": "tinei",
          "contractAmount": 10,
          "createTime": Date.now(),
          "projectId": 10,
          "projectName": "projectName",
          "projectStatus": "projectStatus",
          "receiveDirector": "receiveDirector",
          "receivedAmount": 10,
          "remark": "remark",
          "totalReceivableAmount": 10,
          "totalReceivedAmount": 10,
          "updateTime": Date.now(),
          "userName": "userName"
        },
        {
          "businessType": "tinei",
          "contractAmount": 10,
          "createTime": Date.now(),
          "projectId": 110,
          "projectName": "projectName",
          "projectStatus": "projectStatus",
          "receiveDirector": "receiveDirector",
          "receivedAmount": 10,
          "remark": "remark",
          "totalReceivableAmount": 10,
          "totalReceivedAmount": 10,
          "updateTime": Date.now(),
          "userName": "userName"
        },
      ],
      "totalCount": 0,
      "totalPageNum": 0
    },
    "desc": ""
  }
  return res.json(result)
};

/* 修改计划 */
const editPlan = (req: Request, res: Response, u: string) => {
  const result = {
    "code": 200,
    "desc": ""
  }
  return res.json(result)
};

export default {
  'GET /project/receivePlan/page': receivePlan,
  'POST /receivePlan': editPlan,
};
