// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 获取计划列表 */
export async function getReceivePlan(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/project/receivePlan/page', {
    method: 'GET',
    params: {
      ...params,
    },
    otherParams: options
  });
}

/** 修改计划列表 */
export async function editPlan(
  data: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/receivePlan', {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

