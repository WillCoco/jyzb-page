import { message, Select, Button } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { EditableProTable } from '@ant-design/pro-table';
import withAccess from '@/utils/withAccess';
import makeColumns from '@/utils/columns/makeColumns';
import type { FormValueType } from './components/UpdateForm';
import { getReceivePlan, editPlan } from './service';
import moment from 'moment';
import { getExportExcel } from '@/services/v1/api';
import useProTable from '@/utils/useProTable';
import download from '@/utils/download';
import PopoverIcon from '@/components/PopoverIcon';

const EditIncomeBtn = withAccess('plan_edit', 'a');
const ExportBtn = Button;

const handleUpdate = async (fields: FormValueType, currentRow?: any) => {
  try {
    await editPlan({
      ...currentRow,
      ...fields,
    });
    message.success('修改成功');
    return true;
  } catch (error) {
    return false;
  }
};

const ReceivablesList: React.FC = (props) => {
  // 年份
  const currentMoment = moment();
  const currentYear = currentMoment.format('YYYY');
  const currentMonth = currentMoment.format('M');
  const nextMonth = currentMoment.clone().add(1, 'months').format('M');
  const monthOptions = [
    {label: `${currentMonth}月`, value: nextMonth},
    {label: `${nextMonth}月`, value: nextMonth},
  ]

  // 选中月
  const [month, setMonth] = useState<string>(currentMoment.format('M'));

  const actionRef = useRef<ActionType>();

  // 编辑row
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  const columnsMainBase: ProColumns<any>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      editable: false,
    },
    {
      title: '业务类型',
      dataIndex: 'businessType',
      editable: false,
    },
    {
      title: '项目状态',
      dataIndex: 'projectStatus',
      editable: false,
    },
    {
      title: '项目类型',
      dataIndex: 'projectType',
      hideInForm: true,
      editable: false,
    },
    {
      title: '单位名称',
      dataIndex: 'partnerName',
      editable: false,
    },
    {
      title: '合同金额',
      dataIndex: 'contractAmount',
      editable: false,
    },
    {
      title: '应收账款（合计）',
      dataIndex: 'totalReceivableAmount',
      width: 160,
      editable: false,
      sorter: false
    },
    {
      title: '已收金额（合计）',
      dataIndex: 'totalReceivedAmount',
      width: 160,
      editable: false,
      sorter: false
    },
    {
      title: '本月计划收款',
      dataIndex: 'receivedAmount',
      editable: false,
    },
    {
      title: '收款负责人',
      dataIndex: 'receiveDirector',
      editable: false,
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      editable: false,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      width: 180,
      valueType: 'dateTime',
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      width: 180,
      valueType: 'dateTime',
    },
    {
      title: '操作',
      valueType: 'option',
      width: 120,
      fixed: 'right',
      render: (_, record) => [
        <PopoverIcon
          key="edit"
          accessKey="plan_edit"
          popoverProps={{
            content: "编辑",
          }}
          onClick={() => {
            setEditableRowKeys([record?.id])
          }}
        />,
        // <EditIncomeBtn
        //   key="edit"
        //   style={{marginRight: 8}}
        //   onClick={() => {
        //     setEditableRowKeys([record?.id])
        //   }}
        // >
        //   编辑
        // </EditIncomeBtn>
      ],
    },
  ];

  const columnsMain = makeColumns(columnsMainBase, {
    search: ['projectName', 'projectType', 'businessType', 'projectStatus'],
    edit: ['remark'],
    // hideInTable: ['businessType']
  })

  const {
    onChange,
    orderByType,
    sortField,
    ref
  } = useProTable();

  return (
    <PageContainer
      extra={[
        <Select
          key="select"
          options={monthOptions}
          defaultValue={currentMonth+'月'}
          onChange={(v) => {
            setMonth(v)
          }}
          style={{width: 100}}
        />
      ]}
    >
      <EditableProTable<any, any>
        bordered
        headerTitle="收款计划列表"
        actionRef={actionRef}
        rowKey="id"
        columns={columnsMain}
        onChange={onChange}
        search={{
          span: 4,
          collapsed: false,
          optionRender: (searchConfig, formProps, dom) => {
              return [...dom, (
                <ExportBtn
                  key="export_button"
                  type="primary"
                  onClick={() => {
                    const fields = ref.current?.getFieldsValue() || {};
                    getExportExcel(`/project/receivePlan/export`, {
                      ...fields,
                      orderByType,
                      sortField
                    })
                      .then(download)
                  }}>
                  导出
                </ExportBtn>
              )]
            }
        }}
        // toolBarRender={false}
        request={getReceivePlan}
        params={{year: currentYear, month}}
        postData={(data: { recordList: any; }) => {
          return data?.recordList || [];
        }}
        editable={{
          type: 'single',
          editableKeys,
          onSave: async (rowKey: any, data: any) => {
            const {id, remark} = data || {} as any;
            const success = await handleUpdate({
              month,
              projectId: id,
              remark
            });
            actionRef.current?.reload();
          },
          onChange: setEditableRowKeys,
        }}
        recordCreatorProps={{
          style: {
            display: 'none',
          }
        }}
        scroll={{x: 'max-content', y: 500}}
        pagination={{}}
      />
    </PageContainer>
  );
};

export default ReceivablesList;
