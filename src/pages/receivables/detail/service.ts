// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 获取全年列表 GET /api/rule */
export async function yearData(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableListItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/api/cash-year', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 应收账款表头合计 */
export async function getReceivableTotal(
  params: {
    year?: number|string;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableListItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/project/receivable/total', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

