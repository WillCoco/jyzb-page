import { Button, message, Input, Drawer, DatePicker } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import { ModalForm, ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import type { FormValueType } from './components/UpdateForm';
import UpdateForm from './components/UpdateForm';
import { yearData, rule, addRule, updateRule, removeRule, getReceivableTotal } from './service';
import moment from 'moment';
// import type { any, TableListPagination } from './data';
/**
 * 添加节点
 *
 * @param fields
 */

const handleAdd = async (fields: any) => {
  const hide = message.loading('正在添加');

  try {
    await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};
/**
 * 更新节点
 *
 * @param fields
 */

const handleUpdate = async (fields: FormValueType, currentRow?: any) => {
  const hide = message.loading('正在配置');

  try {
    await updateRule({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('配置成功');
    return true;
  } catch (error) {
    hide();
    message.error('配置失败请重试！');
    return false;
  }
};
/**
 * 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (selectedRows: any[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  try {
    await removeRule({
      key: selectedRows.map((row) => row.key),
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};

// TODO:此处查询需要自己写，搜索项不在此表中，也可写隐藏列用于搜索

const ReceivablesList: React.FC = () => {
  const currentMoment = moment();
  const lastYearMoment = moment().subtract(1, 'year');

  // 年份
  const [year, setYear] = useState<moment.Moment>(currentMoment);

  // 年总计信息
  const [yearTotal, setYearTotal] = useState<Record<string, any>>();

  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /** 分布更新窗口的弹窗 */

  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [showDetail, setShowDetail] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<any>();

  // 编辑row
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  // 年数据

  // 年数据默认展开
  const [yearDefaultExpanded, setYearDefaultExpanded] = React.useState();

  const columnsMain: ProColumns<any>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      colSize: 0,
      width: 120,
    },
    {
      title: '项目经理',
      dataIndex: 'desc',
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '项目类型',
      dataIndex: 'callNo',
      hideInForm: true,
      width: 100,
    },
    {
      title: '合同造价',
      dataIndex: 'closeAmount',
      width: 120,
    },
    {
      title: '类别',
      dataIndex: 'createAt',
      renderFormItem: () => null,
      width: 120,
    },
    {
      title: '本年合计',
      dataIndex: '1',
      renderFormItem: () => null,
      width: 120,
    },
  ];

  // 二级详情展开
  const expandableTableLevel1 = () => {
    const columnsDetail = [
      {
        title: '支出时间',
        dataIndex: 'payAt',
        valueType: 'sameMonthDateTime',
        width: 180,
        // fixed: 'left',
      },
      {
        title: '实际支出金额',
        dataIndex: 'name',
        valueType: 'amount',
        width: 120,
      },
      {
        title: '计划支出金额',
        dataIndex: 'name',
        valueType: 'amount',
        width: 120,
      },
      {
        title: '资金用于',
        dataIndex: 'name',
        width: 120,
      },
      {
        title: '操作人',
        dataIndex: 'name',
        width: 120,
        editable: false
      },
      {
        title: '创建时间',
        dataIndex: 'name',
        valueType: 'dateTime',
        width: 120,
        editable: false
      },
      {
        title: '更新时间',
        dataIndex: 'name',
        valueType: 'dateTime',
        width: 120,
        editable: false
      },
      {
        title: '操作',
        dataIndex: 'name',
        valueType: 'option',
        width: 240,
        render: (_, record) => [
          <Button
            type="primary"
            key="edit"
            style={{marginRight: 8}}
            onClick={() => {
              console.log(record, '____')
              setEditableRowKeys([record.key])
            }}
          >
            修改
          </Button>,
          <Button
            type="primary"
            key="delete"
            onClick={() => {
            }}
          >
            删除
          </Button>,
        ],
      },
    ];

    return (
      <EditableProTable<any, TableListPagination>
        bordered
        headerTitle="查询表格"
        actionRef={actionRef}
        rowKey="key"
        search={false}
        toolBarRender={false}
        request={rule}
        columns={columnsDetail}
        scroll={{x: 'max-content'}}
        editable={{
          type: 'single',
          editableKeys,
          onSave: async (rowKey, data, row) => {
            console.log(rowKey, data, row);
          },
          onChange: setEditableRowKeys,
        }}
        pagination={false}
      />
    )
  }

  React.useEffect(() => {

  }, [])

  const getData = async (...p) => {
    return Promise.all([
      getReceivableTotal({year: year.format('YYYY')}),
      yearData(...p),
    ])
      .then(([r, r1]: any[]) => {
        if (r?.code === 200 && r1.code === 200) {
          if (r1.data) {
            r1.data = {
              ...r1.data,
              recordList: [, ...r1.data?.recordList||[]]
            }
          }
        }
        return r;
      })
      .catch(console.error)
  }

  return (
    <PageContainer
      extra={[
        <DatePicker
          picker="year"
          defaultValue={currentMoment}
          disabledDate={(currentDate) => {
            return !(currentDate.isSame(currentMoment, 'year') || currentDate.isSame(lastYearMoment, 'year'))
          }}
          onChange={(m, s) => setYear(s)}
        />
      ]}
    >
      <ProTable<any, TableListPagination>
        bordered
        headerTitle="查询表格"
        actionRef={actionRef}
        rowKey="key"
        search={{
          span: 4,
          collapsed: false,
          optionRender: (searchConfig,formProps,dom) => dom
        }}
        toolBarRender={false}
        request={getData}
        postData={(data) => {
          setYearDefaultExpanded([data?.[0]?.key])
          return data
        }}
        columns={columnsMain}
        expandable={{
          expandedRowKeys: yearDefaultExpanded,
          expandedRowRender : expandableTableLevel1
        }}
        scroll={{x: 'max-content'}}
        // pagination={{
        //   pageSize: 14
        // }}
      />
      <ModalForm
        title="新建用户"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as any);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: '规则名称为必填项',
            },
          ]}
          width="md"
          name="name"
          label="角色名称"
        />
      </ModalForm>
      <UpdateForm
        onSubmit={async (value) => {
          const success = await handleUpdate(value, currentRow);

          if (success) {
            handleUpdateModalVisible(false);
            setCurrentRow(undefined);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleUpdateModalVisible(false);
          setCurrentRow(undefined);
        }}
        updateModalVisible={updateModalVisible}
        values={currentRow || {}}
      />

      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.name && (
          <ProDescriptions<any>
            column={2}
            title={currentRow?.name}
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              id: currentRow?.name,
            }}
            columns={columns as ProDescriptionsItemProps<any>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default ReceivablesList;
