// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';

/* 获取一年的现金流 */
const getYearData = (req: Request, res: Response, u: string) => {
  const tableListDataSource: TableListItem[] = [];

  tableListDataSource.push({
    key: 1,
    disabled: 1 % 6 === 0,
    href: 'https://ant.design',
    avatar: [
      'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
      'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
    ][1 % 2],
    name: `TradeCode ${1}`,
    owner: '曲丽丽',
    desc: '这是一段描述',
    callNo: Math.floor(Math.random() * 1000),
    status: (Math.floor(Math.random() * 10) % 4).toString(),
    updatedAt: new Date(),
    createdAt: new Date(),
    progress: Math.ceil(Math.random() * 100),
  });

  const result = {
    data: tableListDataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize: 1,
    current: 1,
  };
  return res.json(result)
};

/* 获取一年的现金流 */
const getYearTotal = (req: Request, res: Response) => {
  const result = {
    "code": 200,
    "data": {
      "contractAmount": 10,
      "lastFyExpectReceivedAmount": 10,
      "lastFyTotalAmount": 0,
      "nextWeekEndDate": "",
      "nextWeekNodeCondition": "",
      "nextWeekRate": "",
      "nextWeekReceivableAmount": 0,
      "receivableAmount": 10,
      "receivedAmount": 10,
      "taxConfirmAmount": 0,
      "thisFyQ1Amount": 10,
      "thisFyQ2Amount": 10,
      "thisFyQ3Amount": 10,
      "thisFyQ4Amount": 10,
      "thisFySurplusQExpectReceivedAmount": 110,
      "thisFyTotalAmount": 10
    },
    "desc": ""
  }
  return res.json(result)
};


export default {
  'GET /api/cash-year': getYearData,
  'GET /project/receivable/total': getYearTotal,
};
