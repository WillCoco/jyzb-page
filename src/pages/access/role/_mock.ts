// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';

function roleList(req: Request, res: Response) {
  const result = {
    "code": 200,
    "data": {
      "currentPage": 1,
      "pageSize": 1,
      "recordList": [
        {
          "btnAuthList": [
      {
        "id": 10,
        "key": "project_add",
        "name": "新增项目"
      },
      {
        "id": 11,
        "key": "project_edit",
        "name": "编辑项目"
      },
      {
        "id": 12,
        "key": "project_del",
        "name": "删除项目"
      },
      {
        "id": 13,
        "key": "confirmIncome_add",
        "name": "新增确认收入"
      },
      {
        "id": 14,
        "key": "confirmIncome_edit",
        "name": "修改确认收入"
      },
      {
        "id": 15,
        "key": "confirmIncome_del",
        "name": "删除收确认收入"
      },
      {
        "id": 16,
        "key": "pay_add",
        "name": "新增新增支出"
      },
      {
        "id": 17,
        "key": "pay_edit",
        "name": "修改确认收入"
      },
      {
        "id": 18,
        "key": "pay_del",
        "name": "删除收确认收入"
      },
      {
        "id": 19,
        "key": "plan_edit",
        "name": "修改收款计划"
      },
      {
        "id": 20,
        "key": "node_add",
        "name": "新增收款节点"
      },
      {
        "id": 21,
        "key": "node_edit",
        "name": "修改收款节点"
      },
      {
        "id": 22,
        "key": "node_del",
        "name": "删除收款节点"
      },
      {
        "id": 23,
        "key": "record_add",
        "name": "新增收款记录"
      },
      {
        "id": 24,
        "key": "record_edit",
        "name": "修改收款记录"
      },
      {
        "id": 25,
        "key": "record_del",
        "name": "删除收款记录"
      },
      {
        "id": 26,
        "key": "role_add",
        "name": "新增角色"
      },
      {
        "id": 27,
        "key": "role_edit",
        "name": "修改角色"
      },
      {
        "id": 28,
        "key": "role_del",
        "name": "删除角色"
      },
      {
        "id": 29,
        "key": "tax_add",
        "name": "新增开票"
      },
      {
        "id": 30,
        "key": "tax_edit",
        "name": "修改开票"
      },
      {
        "id": 31,
        "key": "tax_del",
        "name": "删除开票"
      },
      {
        "id": 32,
        "key": "user_add",
        "name": "新增用户"
      },
      {
        "id": 33,
        "key": "user_edit",
        "name": "修改开票"
      },
      {
        "id": 34,
        "key": "user_del",
        "name": "删除开票"
      },
      {
        "id": 35,
        "key": "user_reset",
        "name": "重置用户密码"
      }
    ],
          "id": '1',
          "menuAuthList": [
            {
              "id": 1,
              "key": "home",
              "name": "首页"
            },
            {
              "id": 2,
              "key": "project",
              "name": "项目管理"
            },
            {
              "id": 3,
              "key": "receive",
              "name": "收款管理"
            },
          ],
          "roleName": "roleNameroleNameroleNameroleName",
        }
      ],
      "totalCount": 20,
      "totalPageNum": 2
    },
    "desc": ""
  };

  return res.json(result);
}

function addRole(req: Request, res: Response) {
  const result = {
    "code": 200,
    "desc": ""
  };
  return res.json(result);
}

function editRole(req: Request, res: Response) {
  const result = {
    "code": 200,
    "desc": ""
  };
  return res.json(result);
}
function delRole(req: Request, res: Response) {
  const result = {
    "code": 200,
    "desc": ""
  };
  return res.json(result);
}

export default {
  'GET /role/page': roleList,
  'POST /role': addRole,
  'PUT /role': editRole,
  'GET /role/1': delRole,
};
