// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 角色列表 GET /api/rule */
export async function roleList(params?: any, options?: { [key: string]: any }, ) {
  return request<Record<string, any>>('/role/page', {
    params,
    method: 'GET',
    otherParams: options
  });
}

/** 新增角色 */
export async function addRole(data?: any, options?: { [key: string]: any }) {
  return request<Record<string, any>>('/role', {
    data,
    method: 'POST',
  });
}

/** 编辑角色 */
export async function editRole(data?: any, options?: { [key: string]: any }) {
  return request<Record<string, any>>('/role', {
    data,
    method: 'PUT',
  });
}

/** 删除角色 */
export async function delRole(id?: any) {
  return request<Record<string, any>>(`/role/${id}`, {
    method: 'DELETE',
  });
}

/** 获取全量权限列表 */
export async function getAccess(authType?: any) {
  return request<Record<string, any>>(`/auth`, {
    method: 'GET',
    params: {
      authType
    }
  });
}
