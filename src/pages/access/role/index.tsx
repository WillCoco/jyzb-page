import { PlusOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import { Button, message, Tag, Drawer, Select, } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import { ModalForm, ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import type { FormValueType } from './components/UpdateForm';
import UpdateForm from './components/UpdateForm';
import { roleList, addRole, editRole, delRole } from './service';
import type { TableListItem, TableListPagination } from './data';
import withAccess from '@/utils/withAccess';
import actionRenderWithAccess from '@/utils/actionRenderWithAccess';


const AddRoleBtn = withAccess('role_add', Button);
const EditUserBtn = withAccess('role_edit', 'a');

const format = (list: any[]) => {
  return (list||[]).map((item: any) => {
    return {
      ...item||{},
      value: `${item?.id}|||${item?.key}`
    }
  })
}

/**
 * 添加节点
 *
 * @param fields
 */
const handleAdd = async (fields: TableListItem) => {
  try {
    await addRole({ ...fields });
    message.success('添加成功');
    return true;
  } catch (error) {
    message.error('添加失败请重试！');
    return false;
  }
};
/**
 * 更新节点
 *
 * @param fields
 */

const handleUpdate = async (fields: FormValueType) => {
  const finalField = {
    ...fields,
    btnAuthList: (fields?.btnAuthList||[]).map((item: any) => {
      const [id, key] = (item?.value||'').split('|||');
      return {
        key,
        id,
        name: item?.label
      }
    }),
    menuAuthList: (fields?.menuAuthList||[]).map((item: any) => {
      const [id, key] = (item?.value||'').split('|||');
      return {
        key,
        id,
        name: item?.label
      }
    }),
  }
  try {
    await editRole(finalField);
    message.success('修改成功');
    return true;
  } catch (error) {
    return false;
  }
};
/**
 * 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (id: string) => {
  if (!id) return true;

  try {
    await delRole(id);
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};

const UserList: React.FC = (props) => {
  const {initialState} = useModel('@@initialState');
  const {menuAccessDicts = [], actionAccessDicts = []} = initialState;

  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);

  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [showDetail, setShowDetail] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TableListItem>();

  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  const columns: ProColumns<TableListItem>[] = [
    {
      title: '角色名称',
      dataIndex: 'roleName',
      ellipsis: true
    },
    {
      title: '菜单权限',
      dataIndex: 'menuAuthList',
      width: '24%',
      // valueType: 'tags',
      // XXX: 全局valueType在字段为obj时有bug
      fieldProps: {
        options: menuAccessDicts.map((v: any) => ({...v, value: `${v?.id}|||${v?.key}`, label: v?.name})),
        maxTagCount: 'responsive',
      },
      render: (text) => {
        return (
          <div style={{maxHeight: 50,  overflow: 'auto'}}>
            {[text].flat(1).map((item) => (
              <Tag key={item?.key} style={{marginBottom: 4}}>{item?.name}</Tag>
            ))}
          </div>
        );
      },
      renderFormItem: (value, props) => {
        return (
          <Select
            {...props?.fieldProps}
            options={menuAccessDicts.map((v: any) => ({...v, value: `${v?.id}|||${v?.key}`, label: v?.name}))}
            maxTagCount='responsive'
            // onChange={(v) => {
            //   props?.fieldProps.onChange(v)
            // }}
            // defaultValue={value}
            labelInValue
            optionLabelProp="name"
            size="small"
            mode="multiple"
          />
        )
      },
    },
    {
      title: '按钮权限',
      dataIndex: 'btnAuthList',
      width: '24%',
      // valueType: 'tags',
      fieldProps: {
        options: actionAccessDicts.map((v: any) => ({...v, value: `${v?.id}|||${v?.key}`, label: v?.name})),
        maxTagCount: 'responsive',
      },
       render: (text) => {
        return (
          <div style={{maxHeight: 50,  overflow: 'auto'}}>
            {[text].flat(1).map((item) => (
              <Tag key={item?.key} style={{marginBottom: 4}}>{item?.name}</Tag>
            ))}
          </div>
        );
      },
      renderFormItem: (value, props) => {
        return (
          <Select
            {...props?.fieldProps}
            options={actionAccessDicts.map((v: any) => ({...v, value: `${v?.id}|||${v?.key}`, label: v?.name}))}
            maxTagCount='responsive'
            // onChange={(v) => {
            //   props?.fieldProps && props.fieldProps.onChange(v)
            // }}
            // defaultValue={value}
            labelInValue
            optionLabelProp="name"
            size="small"
            mode="multiple"
          />
        )
      },
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      ellipsis: true,
      renderFormItem: () => null
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      renderFormItem: () => null
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      renderFormItem: () => null
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (__, record) => [
        <EditUserBtn
          key="config"
          onClick={() => {
            setEditableRowKeys([record?.id])
          }}
        >
          编辑
        </EditUserBtn>,
      ],
    }
  ];

  return (
    <PageContainer>
      <EditableProTable<TableListItem, TableListPagination>
        headerTitle="角色列表"
        actionRef={actionRef}
        rowKey="id"
        search={false}
        toolBarRender={() => {
          return (
            [<AddRoleBtn
              type="primary"
              key="primary"
              onClick={() => {
                handleModalVisible(true);
              }}
              style={{marginBottom: 12}}
            >
              <PlusOutlined /> 新增角色
            </AddRoleBtn>]
          )
        }}
        request={roleList}
        postData={(v) => {
          const l = (v?.recordList || []).map((d: any) => ({
            ...d,
            btnAuthList: format(d?.btnAuthList),
            menuAuthList: format(d?.menuAuthList),
          }));

          return l;
        }}
        columns={columns}
        options={{
          search: false,
          fullScreen: false,
          reload: false,
          density: false,
          setting: true
        }}
        editable={{
          type: 'single',
          editableKeys,
          onChange: setEditableRowKeys,
          onSave: async(v, r, newLine) => {
            const success = await handleUpdate(r);
            actionRef.current?.reload();
          },
          onDelete: async(v, r) => {
            const success = await handleRemove(v);
            actionRef.current?.reload();
          },
          actionRender: (row: any, config: any, defaultDom: any) => {
            return actionRenderWithAccess([
              {dom: defaultDom.save, access: props.accessKeySet.has('role_edit')},
              {dom: defaultDom.delete, access: props.accessKeySet.has('role_del')},
              {dom: defaultDom.cancel, access: true},
            ])
          },
        }}
        recordCreatorProps={{
          style: {
            display: 'none'
          }
        }}
        pagination={{}}
        scroll={{y: 510}}
      />
      <ModalForm
        title="新建角色"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as TableListItem);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: '角色名称为必填项',
            },
          ]}
          width="md"
          name="roleName"
          label="角色名称"
        />
      </ModalForm>
      <UpdateForm
        onSubmit={async (value) => {
          const success = await handleUpdate(value, currentRow);

          if (success) {
            handleUpdateModalVisible(false);
            setCurrentRow(undefined);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleUpdateModalVisible(false);
          setCurrentRow(undefined);
        }}
        updateModalVisible={updateModalVisible}
        values={currentRow || {}}
      />
      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.name && (
          <ProDescriptions<TableListItem>
            column={2}
            title={currentRow?.name}
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              id: currentRow?.name,
            }}
            columns={columns as ProDescriptionsItemProps<TableListItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default UserList;
