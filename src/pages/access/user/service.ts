// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 获取用户列表 GET /user/page */
export async function userList(params?: Record<string, any>, otherParams?: { [key: string]: any }) {
  return request<Record<string, any>>('/user/page', {
    params,
    method: 'GET',
    otherParams,
  });
}

/** 新增用户 */
export async function addUser(data?: Record<string, any>, options?: { [key: string]: any }) {
  let finalData = {...data||{}};
  if (finalData.id) {
    finalData.roleId = finalData.id;
    delete finalData.id;
  }
  return request<Record<string, any>>('/user', {
    data: finalData,
    method: 'POST',
  });
}

/** 编辑 */
export async function editUser(data?: Record<string, any>, options?: { [key: string]: any }) {
  return request<Record<string, any>>('/user', {
    data,
    method: 'PUT',
  });
}

/** 编辑 */
export async function delUser(id: string, options?: { [key: string]: any }) {
  return request<Record<string, any>>(`/user/${id}`, {
    method: 'DELETE',
  });
}

/** 修改密码 */
export async function resetPassword(data?: Record<string, any>, options?: { [key: string]: any }) {
  return request<Record<string, any>>('/user/resetPassword', {
    data,
    method: 'POST',
    ...(options || {}),
  });
}
