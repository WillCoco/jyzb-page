import { useModel } from 'umi';
import { PlusOutlined } from '@ant-design/icons';
import { Button, message, Input, Drawer, Popconfirm } from 'antd';
import type { FormInstance } from 'antd';
import _ from 'lodash';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import { ModalForm, BetaSchemaForm, ProFormText, ProFormTextArea, ProFormSelect } from '@ant-design/pro-form';
import withAccess from '@/utils/withAccess';
import { userList, addUser, editUser, delUser, resetPassword } from './service';
import type { TableListItem, TableListPagination } from './data';
import type { DictType } from '@/types/project';

const AddUserBtn = withAccess('user_add', Button)
const EditUserBtn = withAccess('user_edit', 'a')
const ResetUserBtn = withAccess('user_reset', (props: any) => (
  <Popconfirm onConfirm={props.onClick} title="确认重置？">
    <a>重置密码</a>
  </Popconfirm>
), 'hide')
const DelUserBtn = withAccess('user_del', 'a')

/**
 * 更新密码
 * @param fields
 */
const handleUpdatePassword = async (id: string) => {
  try {
    const res = await resetPassword({
      id,
    });

    if (res.code === 200) {
      message.success('修改成功！');
      return true;
    }
    return true;
  } catch (error) {
    // message.error('修改失败请重试！');
    return false;
  }
};

/**
 * 更新节点
 * @param fields
 */

// const handleUpdate = async (fields: FormValueType, currentRow?: TableListItem) => {
//   const hide = message.loading('正在配置');

//   try {
//     // await updateRule({
//     //   ...currentRow,
//     //   ...fields,
//     // });
//     hide();
//     message.success('配置成功');
//     return true;
//   } catch (error) {
//     hide();
//     message.error('配置失败请重试！');
//     return false;
//   }
// };
/**
 * 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (id: string) => {
  if (!id) return true;
  try {
    await delUser(id);
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    return false;
  }
};

const RoleList: React.FC = () => {
  const { initialState } = useModel('@@initialState');
  const { dicts }: {dicts: Record<DictType, any>} = initialState || {};

  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);

  // 编辑窗
  const [editModalVisible, handleEditModalVisible] = useState<boolean>(false);

  // 编辑行
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  // 选中行
  const [currentRow, setCurrentRow] = useState<TableListItem>();

  // 编辑弹窗ref
  const editForm = React.useRef<FormInstance>();

  const actionRef = useRef<ActionType>();

  console.log(dicts.ROLES, 'dicts.ROLES')

  const columns: ProColumns<Record<string, any>>[] = [
    {
      title: '用户账号',
      dataIndex: 'username',
      formItemProps: {
        rules: [{required:  true, message: '此项为必填项'}]
      }
    },
    {
      title: '真实姓名',
      dataIndex: 'realName',
      formItemProps: {
        rules: [{required:  true, message: '此项为必填项'}]
      }
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      hideInSearch: true,
      fieldProps: {
        maxLength: 11
      },
      formItemProps: {
        rules: [{required:  true, message: '此项为必填项'}]
      }
    },
    {
      title: '角色',
      dataIndex: 'roleId',
      valueType: 'select',
      valueEnum: dicts.ROLES,
      formItemProps: {
        rules: [{required:  true, message: '此项为必填项'}]
      },
    },
    {
      title: '用户状态',
      dataIndex: 'enabled',
      valueType: 'select',
      fieldProps: {
        options: [
          {
            label: '正常',
            value: true,
          },
          {
            label: '作废',
            value: false,
          },
        ]
      },
      formItemProps: {
        rules: [{required:  true, message: '此项为必填项'}]
      }
    },
    {
      title: '密码',
      dataIndex: 'password',
      sorter: true,
      valueType: 'password',
      hideInSearch: true,
      hideInTable: true,
      hideInSetting: true,
      hideInForm: true
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      ellipsis: true,
      renderFormItem: () => null
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      renderFormItem: () => null
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      renderFormItem: () => null
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (t, record) => [
        <EditUserBtn
          key="config"
          onClick={() => {
            setEditableRowKeys([record.id])
          }}
        >
          编辑
        </EditUserBtn>,
        <ResetUserBtn
          key="pwd"
          onClick={async() => {
            const success = await handleUpdatePassword(record?.id);
            actionRef.current?.reload();
          }}
        />
      ],
    },
  ];

  return (
    <PageContainer>
      <EditableProTable<TableListItem, TableListPagination>
        headerTitle="用户列表"
        actionRef={actionRef}
        rowKey="id"
        search={{
          span: {
            xs: 24,
            sm: 24,
            md: 12,
            lg: 12,
            xl: 6,
            xxl: 4,
          },
          optionRender: (searchConfig, formProps, dom) => {
            return dom
          }
        }}
        // toolBarRender={false}
        toolBarRender={() => [
          <AddUserBtn key="primary" type="primary" onClick={() => handleModalVisible(true)}>
            <PlusOutlined />
            新建
          </AddUserBtn>,
        ]}
        request={userList}
        postData={(v) => {
          return  (v?.recordList || []).map((d: any) => ({...d||{}, roleId: `${d?.roleId||''}`}))
        }}
        columns={columns}
        editable={{
          type: 'single',
          editableKeys,
          onChange: setEditableRowKeys,
          onSave: (key: React.Key, row, newLine) => {
            console.log(newLine, '新增参数', row)
            const {id, enabled, phone, realName, roleId, username} = row||{} as any;
            editUser({
              id: +id, enabled, phone, realName, roleId, username
            })
              .then((res) => {
                if (res?.code === 200) {
                  message.success('修改成功!');
                  actionRef.current?.reload();
                }
              })
              .catch(console.error)
              .finally(() => {
              })
          },
          onDelete: async (id) => {
            const success = await handleRemove(id);
            if (success) {
              actionRef.current?.reload();
            }
            return success;
          }
        }}
        recordCreatorProps={{
          style: {
            display: 'none',
          },
        }}
        options={{
          fullScreen: false,
          reload: false,
          setting: false,
          density: false
        }}
        pagination={{}}
        scroll={{y: 510}}
      />
      <ModalForm
        title="新建用户"
        width={520}
        layout="horizontal"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 12 }}
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const res = await addUser(value as TableListItem);
          if (res?.code === 200) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <BetaSchemaForm
          layoutType="Embed"
          columns={columns}
          labelAlign="right"
          // onFinish={async (values) => {
          //   console.log(values);
          // }}
        />
      </ModalForm>
      <ModalForm
        title="修改密码"
        formRef={editForm}
        initialValues={currentRow}
        width={520}
        layout="horizontal"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 12 }}
        visible={editModalVisible}
        onVisibleChange={handleEditModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdatePassword(currentRow?.id);
          if (success) {
            handleEditModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          label="密码"
          name="password"
          rules={[{ required: true, message: '请输入密码' }]}
          fieldProps={{
            type: 'password'
          }}
          placeholder="请输入密码"
        />
      </ModalForm>
    </PageContainer>
  );
};

export default RoleList;
