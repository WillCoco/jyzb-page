// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';
import { parse } from 'url';

function getUserList(req: Request, res: Response) {
  const recordList = Array.from(new Array(21)).map((d, i) => {
    return {
      "enabled": true,
      "id": i,
      "phone": "130",
      "realName": "realName",
      "roleName": "roleName0",
      "username": "username"
    }
  })
  const result = {
    "code": 200,
    "data": {
      "currentPage": 0,
      "pageSize": 0,
      "recordList": recordList,
      "totalCount": 0,
      "totalPageNum": 0
    },
    "desc": ""
  }
  return res.json(result);
}

function login(req: Request, res: Response) {
  const result = {
    "code": 0,
    "data": {
      "currentPage": 0,
      "pageSize": 0,
      "recordList": [
        {
          "enabled": true,
          "id": 0,
          "phone": "130",
          "realName": "realName",
          "roleName": "roleName0",
          "username": "username"
        },
        {
          "enabled": true,
          "id": 1,
          "phone": "13109238091",
          "realName": "u1",
          "roleName": "roleName1",
          "username": "username"
        }
      ],
      "totalCount": 0,
      "totalPageNum": 0
    },
    "desc": ""
  }
  return res.json(result);
}

const addUser = (req: Request, res: Response) => {
  const result = {
    code: 200
  }
  return res.json(result);
}

const editUser = (req: Request, res: Response) => {
  const result = {
    code: 200
  }
  return res.json(result);
}

const rPWD = (req: Request, res: Response) => {
  const result = {
    code: 200
  }
  return res.json(result);
}

export default {
  'GET /user/page': getUserList,
  'POST /user': addUser,
  'PUT /user': editUser,
  'POST /user/resetPassword': rPWD,
};
