import React from 'react';
import { Collapse, Space } from 'antd';
import ProTable from '@ant-design/pro-table';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import makeColumns from '@/utils/columns/makeColumns';
import { getCashFlowTotal } from '../service';


function TotalCard(props: {
  typeRender: any,
  combineColumn: any,
  payoutRender: any,
  year: any,
  monthDataFormatter: any,
  actionRef: any
}) {

  const {
    typeRender,
    payoutRender,
    combineColumn,
    monthDataFormatter,
    year,
    actionRef
  } = props;

  const defaultColumnsMain: ProColumns<any>[] = React.useMemo(() => {
    const monthList = new Array(12).fill(undefined)
      .map((d, i) => ({
        title: `${i+1}月`,
        dataIndex: `month_${i+1}`,
        width: 120,
        render: (...p) => payoutRender(undefined, `month_${i+1}`, ...p),
        hideInSearch: true,
        sorter: false
      })) as ProColumns<any>[];

    return [
      {
        title: '类别',
        dataIndex: '',
        width: 120,
        render: typeRender,
        fixed: 'left',
        sorter: false
      },
      ...monthList,
      {
        title: '本年合计',
        dataIndex: 'fyCashAmount',
        valueType: 'amount',
        hideInSearch: true,
        width: 120,
        fixed: 'right',
        sorter: false
      },
    ]
  }, []);

  const columns = makeColumns(defaultColumnsMain);

  const getData = async (...p) => {
    const r = await getCashFlowTotal(...p);
    const data = monthDataFormatter(r?.data);

    console.log(data, 'dataaaaa')
    return {
      ...r||{},
      data
    }
  }

  return (
     <Collapse /* collapsible="header"  */defaultActiveKey={['1']}>
      <Collapse.Panel header={`${year ? year : ''}收款合计`} key="1">
         <ProTable<any, any>
          bordered
          actionRef={actionRef}
          rowKey={() => 'yearTotalCashFLow'}
          search={false}
          toolBarRender={false}
          request={getData}
          params={{year}}
          postData={(data) => {
            return data;
          }}
          columns={columns}
          scroll={{x: 'max-content'}}
          pagination={false}
        />
      </Collapse.Panel>
    </Collapse>
  );
}

export default TotalCard;
