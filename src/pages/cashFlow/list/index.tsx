import { Divider, message, Input, Drawer, DatePicker, Button } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import type { FormValueType } from './components/UpdateForm';
import makeColumns from '@/utils/columns/makeColumns';
import { getCashFlow, addPayout, editPayout, delPayout, getPayout } from './service';
import { getExportExcel } from '@/services/v1/api';
import moment from 'moment';
import TotalTable from './components/TotalTable';
import withAccess from '@/utils/withAccess';
import useProTable from '@/utils/useProTable';
import download from '@/utils/download';

const EditUserBtn = withAccess('pay_edit', 'a')
const ExportBtn = Button;

/**
 * 添加节点
 *
 * @param fields
 */

const handleAdd = async (fields: TableListItem) => {

  try {
    await addPayout({ ...fields });
    message.success('添加成功');
    return true;
  } catch (error) {
    return false;
  }
};
/**
 * 更新节点
 *
 * @param fields
 */

const handleUpdate = async (fields: FormValueType, currentRow?: TableListItem) => {
  try {
    await editPayout({
      ...currentRow,
      ...fields,
    });
    message.success('修改成功');
    return true;
  } catch (error) {
    return false;
  }
};
/**
 * 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (id: string) => {
  if (!id) return true;

  try {
    await delPayout(id);
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};


const CashFlowList: React.FC = (props: any) => {
  const currentMoment = moment();

  const [currentRow, setCurrentRow] = useState<any>([]);
  const [visible, setVisible] = useState<boolean>(false);

  const totalActionRef = React.useRef<ActionType>();

  const actionRef = useRef<ActionType>();

  const actionPayoutRef = useRef<Record<string, ActionType>>({});

  const {
    onChange,
    orderByType,
    sortField,
    ref
  } = useProTable();

  // 编辑row
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  // 年数据

  const [year, setYear] = React.useState<string>(currentMoment.format('YYYY'));

  // 渲染器
  const payoutRender = (names: string[]|false, dataIndex: string, text: React.ReactNode, record: any) => {
    const finalNames = names || ['cashFlowAmount', 'receivedAmount', 'actualPayAmount'];

    return (
      <div>
        <div >{record?.[dataIndex]?.[finalNames[0]] || 0}(万)</div>
        <Divider style={{margin: '4px 0'}} />
        <div >{record?.[dataIndex]?.[finalNames[1]] || 0}(万)</div>
        <Divider style={{margin: '4px 0'}} />
        <div >{record?.[dataIndex]?.[finalNames[2]] || 0}(万)</div>
      </div>
    )
  }

  const typeRender = () => {
    return (
      <div>
        <div /* style={{background: `linear-gradient(to right, #1890ff91, transparent)`, marginBottom: 4}} */>现金流</div>
        <Divider style={{margin: '4px 0'}} />
        <div /* style={{background: `linear-gradient(to right, #fa9e148c, transparent)`, marginBottom: 4}} */>收账</div>
        <Divider style={{margin: '4px 0'}} />
        <div /* style={{background: `linear-gradient(to right, #e2f3d2, transparent)`}} */>支出</div>
      </div>
    )
  }

  // 一级column
  const defaultColumnsLevel1: ProColumns<any>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      width: 120,
    },
    {
      title: '合同金额',
      dataIndex: 'contractAmount',
      valueType: 'amount',
      width: 120,
    },
    {
      title: '结算金额',
      dataIndex: 'closeAmount',
      valueType: 'amount',
      width: 100,
    },
    {
      title: '合计收入(含往年)',
      dataIndex: 'withLastReceivedAmount',
      width: 120,
      valueType: 'amount',
      sorter: false
    },
    {
      title: '合计支出(含往年)',
      dataIndex: 'withLastYearActualPayAmount',
      valueType: 'amount',
      width: 120,
      hideInSearch: true,
      sorter: false
    },
    {
      title: '类别',
      dataIndex: '',
      hideInForm: true,
      hideInSearch: true,
      width: 120,
      render: (t, r) => {
        return <a onClick={() => {
            setCurrentRow(r);
            setVisible(true);
          }
        }>查看详情</a>
      }
    },
    {
      title: '本年合计',
      dataIndex: 'fyCashAmount',
      valueType: 'amount',
      sorter: false,
      hideInForm: true,
      hideInSearch: true,
      width: 120,
    },
  ];

  const columnsLevel1 = makeColumns(defaultColumnsLevel1);

  const columnsDetailBase = [
    {
      title: '支出时间',
      dataIndex: 'payDate',
      valueType: 'date',
      width: 180,
      // fixed: 'left',
    },
    {
      title: '实际支出金额',
      dataIndex: 'actualAmount',
      valueType: 'amount',
      width: 120,
    },
    {
      title: '计划支出金额',
      dataIndex: 'expectAmount',
      valueType: 'amount',
      width: 120,
    },
    {
      title: '资金用于',
      dataIndex: 'plan',
      width: 120,
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      width: 120,
      editable: false
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      width: 120,
      editable: false
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      width: 120,
      editable: false
    },
    {
      title: '操作',
      dataIndex: 'id',
      valueType: 'option',
      width: 240,
      render: (_, record) => [
        <EditUserBtn
          key="edit"
          style={{marginRight: 8}}
          onClick={() => {
            setEditableRowKeys([record.id])
          }}
        >
          编辑
        </EditUserBtn>,
      ],
    },
  ];

  const columnsDetail = makeColumns(columnsDetailBase);

  // 二级详情展开
  const expandableTableLevel2 = (record: any) => {
    return (
      <EditableProTable<any, any>
        bordered
        headerTitle="查询表格"
        actionRef={(ref) => actionPayoutRef.current[record?.id] = ref}
        rowKey="id"
        search={false}
        toolBarRender={false}
        request={getPayout}
        params={{projectId: record?.id}}
        postData={(v) => {
          return v;
        }}
        columns={columnsDetail}
        scroll={{x: 'max-content'}}
        editable={{
          type: 'single',
          editableKeys,
          onSave: async (rowKey, data) => {
            const {id} = record || {};
            // 新增或更新
            const isAdd = `${rowKey||''}`.startsWith('add_');
            const fetchFn = isAdd ? handleAdd : handleUpdate;
            await fetchFn({
              ...data,
              projectId: id,
              index: undefined,
              id: isAdd ? undefined : data.id
            });
            actionPayoutRef.current?.[id]?.reload();
            actionRef.current?.reload();
            totalActionRef.current?.reload();
          },
          onDelete: async (key) => {
            const {id} = record || {};
            await handleRemove(key);
            actionPayoutRef.current?.[id]?.reload();
            actionRef.current?.reload();
            totalActionRef.current?.reload();
          },
          onChange: setEditableRowKeys,
        }}
        recordCreatorProps={{
          record: {
            id: `add_${Date.now()}`,
          },
          // 设置按钮文案
          creatorButtonText: '新增支出',
          ...(props.accessKeySet.has('pay_add') ? {} : {style: {display: 'none'}})
        }}
        pagination={false}
      />
    )
  }

  // 把月份加入
  const combineColumn = (defaultColumns: Record<string, any>[], months: Record<string, any>[], names: string[]|false) => {
    const dC = [...defaultColumns];
    if (!months || months.length === 0) return dC;
    const sMonths = months.map(d => ({
      title: `${d.month}月`,
      dataIndex: `month_${d.month}`,
      width: 120,
      render: (...p) => payoutRender(names, ...p),
      hideInSearch: true
    }));
    dC.splice(-1, 0, ...sMonths);
    return dC;
  }

  // 月份数据format
  const monthDataFormatter = (data: any) => {
    const res: Record<string, any> = {
      fyCashAmount: data?.fyCashAmount
    };
    (data?.cashFlowMonthVoList||[]).forEach((item: any) => {
      if (!item) return;
      res[`month_${item.month}`] = item;
    });
    return [res]
  }

  return (
    <PageContainer
      extra={[
        <DatePicker
          key="year"
          picker="year"
          defaultValue={currentMoment}
          onChange={(m, s) => setYear(s)}
        />
      ]}
    >
      {/* 年度统计 */}
      <TotalTable actionRef={totalActionRef} year={year} combineColumn={combineColumn} monthDataFormatter={monthDataFormatter} payoutRender={payoutRender} typeRender={typeRender} />
      {/* 现金流列表 */}
      <ProTable<any, any>
        bordered
        actionRef={actionRef}
        formRef={ref}
        rowKey="id"
        onChange={onChange}
        search={{
          span: 4,
          collapsed: false,
          labelWidth: 130,
          optionRender: (searchConfig, formProps, dom) => {
            return [...dom, (
              <ExportBtn
                key="export_button"
                type="primary"
                onClick={() => {
                  const fields = ref.current?.getFieldsValue() || {};
                  getExportExcel(`/project/cashFlow/export`, {
                    ...fields,
                    year,
                    orderByType,
                    sortField
                  })
                    .then(download)
                }}>
                导出
              </ExportBtn>
            )]
          }
        }}
        toolBarRender={false}
        request={getCashFlow}
        params={{year}}
        postData={(data) => {
          return data?.recordList || []
        }}
        columns={columnsLevel1}
        expandable={{
          expandedRowRender: expandableTableLevel2
        }}
        scroll={{x: 'max-content', y: 290}}
        // pagination={false}
        style={{marginTop: 24}}
      />
      {/* 抽屉 */}
      <Drawer
        visible={visible}
        onClose={() => setVisible(false)}
        title={ `${year||''}年${currentRow?.projectName}`}
        width={650}
      >
        {
          (currentRow?.cashFlowMonthVoList||[]).map((d: any) => {
            return (
              <React.Fragment key={d?.month}>
                <div style={{display: 'flex', height: 26, justifyContent: 'space-between'}}>
                  <p style={{fontSize: 16, fontWeight: 600, width: 40, marginTop: -2, marginRight: 140}}>{d?.month}月</p>
                  <div style={{display: 'flex', flex: 1, justifyContent: 'flex-end'}}>
                    <p style={{marginRight: 24, width: 100}}>现金流：<p>{d?.cashFlowAmount}(万)</p></p>
                    <p style={{marginRight: 24, width: 100}}>收账：<p>{d?.receivedAmount}(万)</p></p>
                    <p style={{marginRight: 24, width: 100}}>支出：<p>{d?.actualPayAmount}(万)</p></p>
                  </div>
                </div>
                <Divider style={{marginTop: 40}}/>
              </React.Fragment>
            )
          })
        }
      </Drawer>
    </PageContainer>
  );
};

export default CashFlowList;
