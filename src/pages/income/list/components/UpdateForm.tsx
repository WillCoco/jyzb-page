import React from 'react';
import { useModel } from 'umi';
import {
  ModalForm,
  ProFormCheckbox
} from '@ant-design/pro-form';
import type { TableListItem } from '../data';

export type FormValueType = {
  target?: string;
  template?: string;
  type?: string;
  time?: string;
  frequency?: string;
} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const {initialState} = useModel('@@initialState');
  const {menuData = []} = initialState;
  const options: {label: string, value: string}[] = menuData.map((item: any) => ({label: item?.name, value: item?.name}));
  const defaultValue: string[] = menuData.filter((m: any) => m.value).map((m: any) => m.name);
  return (
    <ModalForm
      visible={props.updateModalVisible}
      onFinish={props.onSubmit}
      modalProps={{
        onCancel: () => {
          props.onCancel();
        }
      }}
      initialValues={defaultValue}
    >
      <ProFormCheckbox.Group
        name="radio-vertical"
        label={`菜单权限配置 - ${props.values?.name}`}
        options={options}
      />
    </ModalForm>
  );
};

export default UpdateForm;
