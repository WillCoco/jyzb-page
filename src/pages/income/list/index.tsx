import { useModel } from 'umi';
import { message, DatePicker, Drawer, Divider, Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { EditableProTable } from '@ant-design/pro-table';
import type { FormValueType } from './components/UpdateForm';
import { getIncomeList, getProjectIncome, removeProjectIncome, editProjectIncome, addProjectIncome, updateIncomeTarget } from './service';
import moment, { months } from 'moment';
import type { DictType } from '@/types/project';
import withAccess from '@/utils/withAccess';
import makeColumns, { customPercentValueType, amountValueType } from '@/utils/columns/makeColumns';
import { getExportExcel } from '@/services/v1/api';
import useProTable from '@/utils/useProTable';
import download from '@/utils/download';
import PopoverIcon from '@/components/PopoverIcon';
import { ModalForm, ProFormText } from '@ant-design/pro-form';

const EditIncomeBtn = withAccess('confirmIncome_edit', 'a');
const EditIncomeTargetBtn = withAccess('confirmincome_tar_save', 'a');
const ExportBtn = Button;


// import type { any, TableListPagination } from './data';
/**
 * 添加节点
 *
 * @param fields
 */

const handleAdd = async (fields: any) => {
  try {
    await addProjectIncome({ ...fields });
    message.success('添加成功');
    return true;
  } catch (error) {
    return false;
  }
};
/**
 * 更新节点
 *
 * @param fields
 */

const handleUpdate = async (fields: FormValueType, currentRow?: any) => {
  try {
    await editProjectIncome({
      ...currentRow,
      ...fields,
    });
    message.success('保存成功');
    return true;
  } catch (error) {
    return false;
  }
};
/**
 * 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (id: string|number) => {
  if (!id) return true;

  try {
    await removeProjectIncome({id});
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};

// TODO:此处查询需要自己写，搜索项不在此表中，也可写隐藏列用于搜索

const IncomeList: React.FC = (props: any) => {
  const currentMoment = moment();

  // 年份
  const [year, setYear] = useState<string>(currentMoment.format('YYYY'));

  const { initialState } = useModel('@@initialState');
  const { dicts }: {dicts: Record<DictType, any>} = initialState || {};

  const actionRef = useRef<ActionType>();
  const actionEditRef = useRef<ActionType>();

  // 编辑row
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);

  // setCurrentMonths
  const [currentMonths, setCurrentMonths] = useState<any[]>([]);
  const [visible, setVisible] = useState<boolean>(false);


  // 年数据默认展开
  // const [yearDefaultExpanded, setYearDefaultExpanded] = React.useState();

  const defaultColumnsMainBase: ProColumns<any>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
    },
    {
      title: '项目类型',
      dataIndex: 'projectType',
      width: 100,
      valueType: 'select',
      valueEnum: dicts.PROJECT_TYPE
    },
    {
      title: '业务类型',
      dataIndex: 'businessType',
      valueType: 'select',
      width: 120,
      valueEnum: dicts.BUSINESS_TYPE,
      hideInTable: true
    },
    {
      title: '项目状态',
      dataIndex: 'projectStatus',
      sorter: true,
      width: 120,
      valueEnum: dicts.PROJECT_STATUS,
      hideInTable: true
    },

    {
      title: '项目经理',
      dataIndex: 'dataDirector',
      renderFormItem: () => null
    },

    {
      title: '合同造价',
      dataIndex: 'contractAmount',
      hideInSearch: true,
      valueType: 'amount',
      width: 120,
    },
    {
      title: '结算金额',
      dataIndex: 'closeAmount',
      hideInSearch: true,
      valueType: 'amount',
      width: 120,
    },
    {
      title: `年目标`,
      hideInSearch: true,
      width: 120,
      children: [
        {
          title: `金额`,
          dataIndex: 'fyTaxTargetAmount',
          valueType: 'amount',
          hideInSearch: true,
          width: 120,
          ...amountValueType
        },
        {
          title: `比例`,
          dataIndex: 'fyTargetRate',
          valueType: 'customPercent',
          hideInSearch: true,
          width: 120,
          render: customPercentValueType.render('fyTargetRate'),
          renderFormItem: customPercentValueType.renderFormItem,
        },
        {
          title: `不含税金额`,
          dataIndex: 'fyTargetAmount',
          valueType: 'amount',
          hideInSearch: true,
          width: 120,
          ...amountValueType
        },
      ]
    },
    {
      title: `实时`,
      width: 120,
      hideInSearch: true,
      children: [
        {
          title: `累计金额`,
          dataIndex: 'rtTaxAmount',
          valueType: 'amount',
          hideInSearch: true,
          width: 120,
          ...amountValueType
        },
        {
          title: `累计比例`,
          dataIndex: 'rtRate',
          valueType: 'customPercent',
          width: 120,
          render: customPercentValueType.render('rtRate'),
          renderFormItem: customPercentValueType.renderFormItem,
          sorter: true
        },
        {
          title: `不含税金额`,
          dataIndex: 'rtAmount',
          valueType: 'amount',
          width: 120,
          ...amountValueType
        },
      ]
    },
    {
      title: `剩余`,
      width: 120,
      hideInSearch: true,
      children: [
        {
          title: `金额`,
          dataIndex: 'surplusTaxAmount',
          hideInSearch: true,
          valueType: 'amount',
          width: 120,
          ...amountValueType
        },
        {
          title: `不含税金额`,
          dataIndex: 'surplusAmount',
          hideInSearch: true,
          valueType: 'amount',
          width: 120,
          ...amountValueType
        },
      ]
    },
    {
      title: `${year||''}年度确认完工比例金额`,
      dataIndex: 'monthAmountVoList',
      hideInSearch: true,
      width: 120,
      sorter: false,
      render: (t, r) => {
        return <a onClick={() => {
          setCurrentMonths(r?.monthAmountVoList || []);
          setVisible(true);
          }
        }>查看详情</a>
      }
    },
    {
      title: '备注',
      dataIndex: 'remark',
      hideInSearch: true,
      width: 120,
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      width: 40,
      fixed: 'right',
      render: (__, record) => [
        <ModalForm
          key="config"
          width={375}
          title="确认收入目标"
          modalProps={{
            centered: true,
            // getContainer: () => document.querySelector('#income-target-edit')
          }}
          trigger={(
            // EditIncomeTargetBtn
            <EditIncomeTargetBtn><EditOutlined/></EditIncomeTargetBtn> ||
            <PopoverIcon
              accessKey="confirmincome_tar_save"
              id="income-target-edit"
              popoverProps={{
                content: "新增确收目标",
              }}
              onClick={() => {}}
            />
          )}
          onFinish={async (values) => {
            try {
              await updateIncomeTarget({
                amount: values.amount,
                year,
                projectId: record.id
              });
              message.success('更新确认收入目标成功');
              actionEditRef.current?.reload();
              actionRef.current?.reload();
              return true;
            } catch {
            }
            message.error('更新确认收入目标失败');
            return false
          }}
        >
          <ProFormText
            required
            // label="签约客户名称"
            name="amount"
            tooltip="最长为 24 位"
            placeholder="请输入确认收入目标（万）"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </ModalForm>

      ],
    }
  ];

  const columnsMain = makeColumns(defaultColumnsMainBase, {
    // search: ['projectName', 'projectType', 'businessType', 'project']
  });

  const columnsDetailBase = [
    {
      title: '确认收入节点',
      dataIndex: 'confirmDate',
      width: 120
    },
    {
      title: '确认金额',
      dataIndex: 'confirmAmount',
      width: 120
    },
    {
      title: '累计百分比',
      dataIndex: 'completionRate',
      valueType: 'customPercent',
      width: 120,
      editable: false
    },
    {
      title: '是否已回执',
      dataIndex: 'hasReceipt',
      valueType: 'switch',
      width: 120
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      width: 120
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      width: 120
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      width: 120
    },
    {
      title: '操作',
      valueType: 'option',
      width: 140,
      fixed: 'right',
      render: (_, record) => [
        <EditIncomeBtn
          key="edit"
          style={{marginRight: 8}}
          onClick={() => {
            setEditableRowKeys([record.id])
          }}
        >
          编辑
        </EditIncomeBtn>,
      ],
    },
  ];

  const columnsDetail = makeColumns(columnsDetailBase, {
    required: ['completionRate', 'confirmAmount', 'confirmDate']
  })
  // 二级详情展开
  const expandableTableLevel1 = (record: any) => {
    return (
      <EditableProTable<any, any>
        bordered
        headerTitle="确认收入列表"
        actionRef={actionEditRef}
        rowKey="id"
        search={false}
        toolBarRender={false}
        request={getProjectIncome}
        params={{year, projectId: record?.id}}
        columns={columnsDetail}
        editable={{
          type: 'single',
          editableKeys,
          onSave: async (rowKey, data) => {
            // 添加
            const isAdd = (`${rowKey||''}`).startsWith('add_');
            if (isAdd) {
              const success = await handleAdd({
                ...data,
                projectId: record?.id,
                id: undefined,
                index: undefined,
              });
              actionEditRef.current?.reload();
              actionRef.current?.reload();
              return;
            }
            // 新增和保存开票
            const success = await handleUpdate({
                ...data,
                projectId: record?.id,
                index: undefined,
              });
            actionEditRef.current?.reload();
            actionRef.current?.reload();
          },
          onDelete: async (rowKey) => {
            const success = await handleRemove(rowKey);
            actionEditRef.current?.reload();
            actionRef.current?.reload();
          },
          onChange: setEditableRowKeys,
        }}
        recordCreatorProps={{
          creatorButtonText: '新增确认收入',
          record: {
            id: `add_${Date.now()}`,
          },
          ...(props.accessKeySet.has('confirmIncome_add') ? {} : {style: {display: 'none'}})
        }}
        pagination={false}
        // scroll={{x: 'max-content'}}
      />
    )
  }

  const {
    onChange,
    orderByType,
    sortField,
    ref
  } = useProTable();

  return (
    <PageContainer
      extra={[
        <DatePicker
          key="picker"
          picker="year"
          defaultValue={currentMoment}
          onChange={(m, s) => setYear(s)}
        />
      ]}
    >
      <ProTable<any, any>
        bordered
        headerTitle="查询表格"
        actionRef={actionRef}
        rowKey="id"
        onChange={onChange}
        formRef={ref}
        search={{
          span: 4,
          collapsed: false,
          optionRender: (searchConfig, formProps, dom) => {
            return [...dom, (
              <ExportBtn
                key="export_button"
                type="primary"
                target="blank"
                onClick={() => {
                  const fields = ref.current?.getFieldsValue() || {};
                  getExportExcel(`/project/confirmIncome/export`, {
                    ...fields,
                    year,
                    orderByType,
                    sortField
                  })
                    .then(download)
                }}
                >
                导出
              </ExportBtn>
            )]
          }
        }}
        toolBarRender={false}
        request={getIncomeList}
        params={{
          year
        }}
        postData={(v) => {
          return v?.recordList || []
        }}
        columns={columnsMain}
        expandable={{
          // expandedRowKeys: yearDefaultExpanded,
          expandedRowRender : expandableTableLevel1
        }}
        scroll={{x: 'max-content',  y: 518}}
      />
      <Drawer
        visible={visible}
        onClose={() => setVisible(false)}
        title={ `${year||''}年度确认完工比例金额`}
        width={450}
      >
        {
          (currentMonths||[]).map(d => {
            return (
              <React.Fragment key={d?.month}>
                <div style={{display: 'flex', height: 26}}>
                  <p style={{fontSize: 16, fontWeight: 600, width: 40, marginTop: -2, marginRight: 80}}>{d?.month}月</p>
                  <p style={{marginRight: 24}}>金额：{d?.amount}(万)</p>
                  <p>比例：{((d?.rate||0) * 100).toFixed(2)}%</p>
                </div>
                <Divider />
              </React.Fragment>
            )
          })
        }
      </Drawer>
    </PageContainer>
  );
};

export default IncomeList;
