// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';


/* 获取一年的现金流 */
const getYearData = (req: Request, res: Response, u: string) => {
  const tableListDataSource: TableListItem[] = [];

  tableListDataSource.push({
    key: 1,
    disabled: 1 % 6 === 0,
    href: 'https://ant.design',
    avatar: [
      'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
      'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
    ][1 % 2],
    name: `TradeCode ${1}`,
    owner: '曲丽丽',
    desc: '这是一段描述',
    callNo: Math.floor(Math.random() * 1000),
    status: (Math.floor(Math.random() * 10) % 4).toString(),
    updatedAt: new Date(),
    createdAt: new Date(),
    progress: Math.ceil(Math.random() * 100),
  });

  const result = {
    data: tableListDataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize: 1,
    current: 1,
  };
  return res.json(result)
};

/* 获取确收 */
const confirmIncome = (req: Request, res: Response, u: string) => {
  const result = {
    "code": 200,
    "data": {
      "currentPage": 0,
      "pageSize": 0,
      "recordList": [
        {
          "contractAmount": 10,
          "dataDirector": "1",
          "fyTargetAmount": 10,
          "fyTargetRate": "1",
          "fyTaxTargetAmount": 10,
          "monthAmountVoList": [
            {
              "amount": 10,
              "month": 10,
              "rate": "1"
            },
            {
              "amount": 10,
              "month": 9,
              "rate": "1"
            },
            {
              "amount": 10,
              "month": 8,
              "rate": "1"
            },

          ],
          "projectId": 1,
          "projectName": "projectName",
          "projectType": "tinei",
          "remark": "111",
          "rtAmount": 0,
          "rtRate": "1",
          "rtTaxAmount": 0,
          "surplusAmount": 0,
          "surplusTaxAmount": 0
        }
      ],
      "totalCount": 0,
      "totalPageNum": 0
    },
    "desc": ""
  };
  return res.json(result)
};

/* 获取单个项目确收 */
const projectIncome = (req: Request, res: Response, u: string) => {
  const result = {
    "code": 200,
    "data": [
      {
        "completionRate": "1",
        "confirmAmount": 0,
        "confirmDate": "1",
        "createTime": Date.now(),
        "hasReceipt": true,
        "id": 10,
        "projectId": 10,
        "updateTime": Date.now(),
        "userId": "userId",
        "userName": "userName"
      }
    ],
    "desc": ""
  }
    return res.json(result)
  };

export default {
  'GET /api/cash-year': getYearData,
  'GET /project/confirmIncome/page': confirmIncome,
  'GET /confirmIncome': projectIncome,
};
