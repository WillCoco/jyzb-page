// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 获取所有项目确收列表 */
export async function getIncomeList(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/project/confirmIncome/page', {
    method: 'GET',
    params: {
      ...params,
    },
    otherParams: options
  });
}

/** 获取一个项目确收列表 */
export async function getProjectIncome(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/confirmIncome', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 添加 */
export async function addProjectIncome(
  data: any,
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/confirmIncome', {
    method: 'POST',
    data,
  });
}

/** 修改 */
export async function editProjectIncome(
  data: any,
  options?: { [key: string]: any },
) {
  return request<{
    data: any;
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  }>('/confirmIncome', {
    method: 'PUT',
    data,
    ...(options || {}),
  });
}

/** 删除 */
export async function removeProjectIncome(
  params: any,
) {
  return request<{
    data: any;
    total?: number;
    success?: boolean;
  }>(`/confirmIncome/${params?.id}`, {
    method: 'DELETE',
  });
}



/** 更新确收目标 */
export async function updateIncomeTarget(
  data: any,
) {
  return request<{
    data: any;
    total?: number;
    success?: boolean;
  }>(`/confirmIncome/tar`, {
    method: 'POST',
    data
  });
}


