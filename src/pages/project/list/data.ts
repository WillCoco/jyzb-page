export type TableListItem = {
	"closeAmount": number,
  "confirmAmount": number,
  "confirmContractRate": number,
  "confirmReceivableDiffAmount": number,
  "contractAmount": number,
  "contractDate": string,
  "contractRate": number,
  "createTime": string,
  "dataDirector": string,
  "endDate": string,
  "id": number,
  "isBadDebt": true,
  "lastFyExpectReceivedAmount": number,
  "partnerName": string,
  "projectName": string,
  "projectStatus": number,
  "projectType": string,
  "receivableAmount": number,
  "receiveCycle": number,
  "receiveDirector": string,
  "receivedAmount": number,
  "remark": string,
  "startDate": string,
  "taxPoint": string,
  "taxUnreceiveAmount": number,
  "thisFySurplusQExpectReceivedAmount": number,
  "updateTime": string,
  "userName": string,
  "workDay": number
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
