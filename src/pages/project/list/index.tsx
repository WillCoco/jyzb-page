import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { Link, useHistory, useModel } from 'umi';
import { Button, Upload, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { projectList } from './service';
import type { TableListItem, TableListPagination } from './data';
import { DictType } from '@/types/project';
import withAccess from '@/utils/withAccess';
import makeColumns from '@/utils/columns/makeColumns';
import { getExportExcel } from '@/services/v1/api';
import useProTable from '@/utils/useProTable';
import download from '@/utils/download';
import PopoverIcon from '@/components/PopoverIcon';

const NewBtn = withAccess('project_add', Button);
const ImportBtn = withAccess('project_import', Button);
const ExportBtn = Button;
const AddNodeBtn = withAccess('node_add', 'a');
const EditNodeBtn = withAccess('node_edit', 'a');


const ProjectList: React.FC = () => {
  const history = useHistory();
  const actionRef = useRef<ActionType>();
  const [editableKeys, setEditableKeys] = useState<React.Key[]>();

  const { initialState } = useModel('@@initialState');
  const { dicts }: {dicts: Record<DictType, any>} = initialState || {};

  const columnsBase: ProColumns<TableListItem>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      width: 180,
      sorter: true,
      ellipsis: true,
      render: (t, r) => (
        <Link className="project-link" to={{
          pathname: `/project/list/detail/${r.id}`,
        }}>
          {t}
        </Link>
      )
    },
    {
      title: '甲方名称',
      dataIndex: 'partnerName',
      valueType: 'textarea',
      sorter: true,
      width: 120,
      // fixed: 'left',
      renderFormItem: () => null
    },
    {
      title: '项目类型',
      dataIndex: 'projectType',
      hideInForm: true,
      valueType: 'select',
      sorter: true,
      width: 100,
      valueEnum: dicts.PROJECT_TYPE
    },
    {
      title: '业务类型',
      dataIndex: 'businessType',
      valueType: 'select',
      sorter: true,
      width: 120,
      valueEnum: dicts.BUSINESS_TYPE
    },
    {
      title: '税点',
      dataIndex: 'taxPoint',
      valueType: 'customPercent',
      renderFormItem: () => null,
      sorter: true,
      width: 120,
    },
    {
      title: '合同金额',
      dataIndex: 'contractAmount',
      valueType: 'amount',
      renderFormItem: () => null,
      sorter: true,
      width: 120,
    },
    {
      title: '结算金额',
      dataIndex: 'closeAmount',
      valueType: 'amount',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '预计收款(本年剩余季度)',
      dataIndex: 'thisFySurplusQExpectReceivedAmount',
      valueType: 'amount',
      sorter: false,
      width: 180,
      renderFormItem: () => null
    },
    {
      title: '预计收款(次年总计)',
      dataIndex: 'lastFyExpectReceivedAmount',
      valueType: 'amount',
      sorter: false,
      width: 180,
      renderFormItem: () => null
    },
    {
      title: '应收金额',
      dataIndex: 'receivableAmount',
      valueType: 'amount',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '已收金额',
      dataIndex: 'receivedAmount',
      valueType: 'amount',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '已开票未到账',
      dataIndex: 'taxUnreceiveAmount',
      valueType: 'amount',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '开票总额',
      dataIndex: 'taxAmount',
      valueType: 'amount',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '合同/结算收款比例',
      dataIndex: 'contractRate',
      valueType: 'customPercent',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '项目状态',
      dataIndex: 'projectStatus',
      sorter: true,
      width: 120,
      valueEnum: dicts.PROJECT_STATUS
    },
    {
      title: '确认收入金额与应收金额差',
      dataIndex: 'confirmReceivableDiffAmount',
      valueType: 'amount',
      sorter: true,
      width: 240,
      renderFormItem: () => null
    },
    {
      title: '确认收入进度与合同/结算收款比例',
      dataIndex: 'confirmContractRate',
      valueType: 'customPercent',
      sorter: true,
      width: 240,
      renderFormItem: () => null
    },
    {
      title: '合同签订日期',
      dataIndex: 'contractDate',
      valueType: 'date',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '工期',
      dataIndex: 'workDay',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '结算周期',
      dataIndex: 'closeCycle',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '结算日期',
      dataIndex: 'closeDate',
      valueType: 'date',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '坏账',
      dataIndex: 'isBadDebt',
      valueType: 'select',
      sorter: true,
      width: 120,
      renderText: (v) => v ? '是' : '否',
      renderFormItem: () => null
    },
    {
      title: '收款周期',
      dataIndex: 'receiveCycle',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '开工时间',
      dataIndex: 'startDate',
      valueType: 'date',
      hideInSearch: true,
      sorter: true,
      width: 120,
    },
    {
      title: '完工时间',
      dataIndex: 'endDate',
      valueType: 'date',
      sorter: true,
      hideInSearch: true,
      width: 120,
    },
    {
      title: '备注',
      dataIndex: 'remark',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      sorter: true,
      width: 120,
      renderFormItem: () => null
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      sorter: true,
      width: 200,
      renderFormItem: () => null
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      sorter: true,
      width: 200,
      renderFormItem: () => null
    },
    {
      title: '操作',
      valueType: 'option',
      width: 60,
      fixed: 'right',
      render: (_, record) => [
        <PopoverIcon
          key="edit"
          accessKey="node_add"
          popoverProps={{
            content: "编辑项目信息",
          }}
          onClick={() => {
            history.push({
              pathname: `/project/list/edit/${record?.id}`,
              state: {
                type: 'edit',
                record
              }
            })
          }}
        />,
        <PopoverIcon
          key="addNodes"
          accessKey="node_edit"
          icon={<FormOutlined />}
          popoverProps={{
            content: "编辑收款节点",
          }}
          onClick={() => {
            history.push({
              pathname: `/project/list/edit/${record?.id}`,
              state: {
                type: 'addNode'
              }
            })
          }}
        />,
      ],
    },
  ];

  const columns = makeColumns(columnsBase);

  const {
    onChange,
    orderByType,
    sortField,
    ref
  } = useProTable();

  return (
    <PageContainer/*  title={false} */>
      <ProTable<TableListItem, TableListPagination>
        headerTitle="查询表格"
        actionRef={actionRef}
        rowKey="id"
        onChange={onChange}
        search={{
          span: 4,
          collapsed: false,
          optionRender: (searchConfig, formProps, dom) => {
            return [...dom, (
              <ExportBtn
                key="export_button"
                type="primary"
                onClick={() => {
                  const fields = ref.current?.getFieldsValue() || {};
                  getExportExcel(`/project/export`, {
                    ...fields,
                    orderByType,
                    sortField
                  })
                    .then(download)
                }}
                >
                导出
              </ExportBtn>
            )]
          }
        }}
        toolBarRender={() => [
          <NewBtn
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              history.push({
                pathname: '/project/list/add',
                state: {
                  type: 'new'
                }
              })
            }}>
            新建
          </NewBtn>,
          <Upload
            key="import"
            showUploadList={false}
            name="file"
            action={`/api/v1/project/importExcel`}
            headers={{
              consumes: 'multipart/form-data'
            }}
            onChange={
              (info) => {
                console.log(info, '上传文件')
                if (info.file.status === 'done') {
                  if (info.file?.response?.code === 200) {
                    message.success(`导入成功`);
                  } else {
                    message.error(info.file?.response?.desc || `导入失败`);
                  }
                } else if (info.file.status === 'error') {
                  message.error(`导入失败`);
                }
              }
            }
          >
            <ImportBtn
              icon={<PlusOutlined />}
              type="primary"
            >
              导入
            </ImportBtn>
          </Upload>
        ]}
        // editable={{
        //   type: 'single',
        //   editableKeys,
        //   onSave: async (rowKey, data, row) => {
        //     console.log(rowKey, data, row, '保存单行');
        //   },
        //   onChange: setEditableKeys,
        // }}
        request={projectList}
        postData={(d: any) => d?.recordList||[]}
        columns={columns}
        scroll={{x: 'max-content', y: 480}}
      />
     </PageContainer>
  );
};

export default ProjectList;
