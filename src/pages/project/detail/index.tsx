import { CaretRightOutlined } from '@ant-design/icons';
import { Button, Collapse, message, Popconfirm, } from 'antd';
import { useParams, useModel, history } from 'umi';
import React, { useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import type { TableListItem, TableListPagination } from './data';
import { ProjectTypeEnum, ProjectStatusEnum, BusinessTypeEnum } from '@/types/project';
import type { DictType } from '@/types/project';
import { projectDetail, removeProject } from './service';
import withAccess from '@/utils/withAccess';
import makeColumns from '@/utils/columns/makeColumns';
import { getNodes } from '../../receivables/list/service';

const handleRemoveProject = async (id: string|number) => {
  if (!id) return true;

  try {
    await removeProject({id});
    message.success('删除成功');
    return true;
  } catch (error) {
    return false;
  }
};

const RoleList: React.FC = () => {
  const {id} = useParams<{id: string}>();

  const { initialState } = useModel('@@initialState');
  const { dicts, currentUser }: {dicts: Record<DictType, any>} = initialState || {};
  const { accessKeySet = new Set() } = currentUser || {};

  const actionRef = useRef<ActionType>();
  const columns1Base: ProColumns<TableListItem>[] = [
    {
      title: '项目名称',
      dataIndex: 'projectName',
      width: 120,
      sorter: false,
    },
    {
      title: '甲方名称',
      dataIndex: 'partnerName',
      valueType: 'textarea',
      width: 120,
      // fixed: 'left',
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '项目类型',
      dataIndex: 'projectType',
      hideInForm: true,
      valueType: 'select',
      width: 100,
      valueEnum: dicts.PROJECT_TYPE,
      sorter: false,
    },
    {
      title: '业务类型',
      dataIndex: 'businessType',
      valueType: 'select',
      width: 120,
      valueEnum: dicts.BUSINESS_TYPE,
      sorter: false,
    },
    {
      title: '税点',
      dataIndex: 'taxPoint',
      valueType: 'customPercent',
      renderFormItem: () => null,
      width: 120,
      sorter: false,
    },
    {
      title: '合同金额',
      dataIndex: 'contractAmount',
      valueType: 'amount',
      renderFormItem: () => null,
      width: 120,
      sorter: false,
    },
    {
      title: '预计收款(本年剩余季度)',
      dataIndex: 'thisFySurplusQExpectReceivedAmount',
      valueType: 'amount',
      width: 180,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '预计收款(次年总计)',
      dataIndex: 'lastFyExpectReceivedAmount',
      valueType: 'amount',
      width: 180,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '应收金额',
      dataIndex: 'receivableAmount',
      valueType: 'amount',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '已收金额',
      dataIndex: 'receivedAmount',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '已开票未到账',
      dataIndex: 'taxUnreceiveAmount',
      valueType: 'amount',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '合同/结算收款比例',
      dataIndex: 'contractRate',
      valueType: 'customPercent',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '项目状态',
      dataIndex: 'projectStatus',
      width: 120,
      valueEnum: dicts.PROJECT_STATUS,
      sorter: false,
    },
    {
      title: '确认收入金额与应收金额差',
      dataIndex: 'confirmReceivableDiffAmount',
      valueType: 'amount',
      width: 240,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '确认收入进度与合同/结算收款比例',
      dataIndex: 'confirmContractRate',
      valueType: 'customPercent',
      width: 240,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '结算金额',
      dataIndex: 'closeAmount',
      valueType: 'amount',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '合同签订日期',
      dataIndex: 'contractDate',
      valueType: 'date',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '工期',
      dataIndex: 'workDay',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '坏账',
      dataIndex: 'isBadDebt',
      valueType: 'select',
      width: 120,
      renderText: v => v ? '是' : '否',
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '收款周期',
      dataIndex: 'receiveCycle',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '开工时间',
      dataIndex: 'startDate',
      valueType: 'date',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '完工时间',
      dataIndex: 'endDate',
      width: 120,
      valueType: 'date',
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '操作人',
      dataIndex: 'userName',
      width: 120,
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      valueType: 'dateTime',
      renderFormItem: () => null,
      sorter: false,
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      valueType: 'dateTime',
      renderFormItem: () => null,
      sorter: false,
    },
  ];

  const columns1 = makeColumns(columns1Base);

  // 收款节点
  const columnsIncomeNodeBase: ProColumns<TableListItem>[] = [
    {
      title: '节点名称',
      dataIndex: 'nodeName',
    },
    {
      title: '计划收款日期',
      dataIndex: 'endDate',
      valueType: 'date'
    },
    {
      title: '收款条件',
      dataIndex: 'nodeCondition',
    },
    {
      title: '计划收款进度',
      dataIndex: 'completionRate',
      sorter: true,
      valueType: 'customPercent',
    },
    {
      title: '款项类型',
      dataIndex: 'type',
      sorter: true,
      valueType: 'select',
      valueEnum: dicts.NODE_TYPE
    },
    {
      title: '计划收款金额',
      dataIndex: 'receivableAmount',
      valueType: 'amount',
    },
    {
      title: '实际收款进度',
      dataIndex: 'actualRate',
      sorter: true,
      valueType: 'customPercent',
      sorter: true,
    },
    {
      title: '实际可收金额',
      dataIndex: 'closeAmount',
      valueType: 'amount',
    },
    {
      title: '实际收款金额',
      dataIndex: 'receivedAmount',
      valueType: 'amount',
    },
    {
      title: '计划差值',
      dataIndex: 'unReceivedAmount',
      valueType: 'amount',
    },
  ];

  const columnsIncomeNode = makeColumns(columnsIncomeNodeBase);

  // 收款记录
  const columnsIncomeRecordBase: ProColumns<TableListItem>[] = [
    {
      title: '收款节点',
      dataIndex: 'receiveNodeName',
      sorter: true,
    },
    {
      title: '到账金额',
      dataIndex: 'amount',
      valueType: 'amount',
      sorter: true,
    },
    {
      title: '到账类型',
      dataIndex: 'honorPayType',
      sorter: true,
      hideInForm: true,
      valueEnum: dicts.HONOR_PAY_TYPE
    },
    {
      title: '支付方式',
      dataIndex: 'payType',
      sorter: true,
      hideInForm: true,
      valueType: 'select',
      valueEnum: dicts.PAY_TYPE
    },
    {
      title: '收款时间',
      valueType: 'date',
      dataIndex: 'receiveDate',
      sorter: true,
    },
    {
      title: '备注',
      dataIndex: 'remak',
    },
  ];

  const columnsIncomeRecord = makeColumns(columnsIncomeRecordBase);

  // 确认收入
  const columnsIncomeBase: ProColumns<TableListItem>[] = [
    {
      title: '确认收入节点',
      dataIndex: 'confirmDate',
      valueType: 'date',
      sorter: true,
    },
    {
      title: '确认收入金额',
      dataIndex: 'confirmAmount',
      valueType: 'amount',
      sorter: true,
    },
    {
      title: '收入进度',
      dataIndex: 'completionRate',
      sorter: true,
      valueType: 'customPercent',
    },
    {
      title: '回执标记',
      dataIndex: 'hasReceipt',
      sorter: true,
      renderText: v => v ? '是' : '否'
    },
  ];

  const columnsIncome = makeColumns(columnsIncomeBase);

  // 现金支出
  const columnsPaidBase: ProColumns<TableListItem>[] = [
    {
      title: '计划支出时间',
      dataIndex: 'payDate',
      valueType: 'date',
      sorter: true,
    },
    {
      title: '计划支出金额',
      dataIndex: 'expectAmount',
      valueType: 'amount',
      sorter: true,
    },
    {
      title: '实际支出时间',
      dataIndex: 'payDate',
      sorter: true,
      valueType: 'date',
    },
    {
      title: '实际支出金额',
      dataIndex: 'actualAmount',
      valueType: 'amount',
      sorter: true,
    },
    {
      title: '支出说明',
      dataIndex: 'plan',
    },
  ];

  const columnsPaid = makeColumns(columnsPaidBase);


  // 当前操作项目的节点map
  const [currentNodeMap, setCurrentNodeMap] = React.useState<any>({});

  // 开票
  const columnsTaxBase: ProColumns<TableListItem>[] = [
    {
      title: '开票日期',
      dataIndex: 'taxDate',
      sorter: false,
    },
    {
      title: '收款节点',
      dataIndex: 'receiveNodeId',
      valueType: 'select',
      valueEnum: currentNodeMap,
      sorter: false,
    },
    {
      title: '发票号码',
      dataIndex: 'taxNo',
      sorter: false,
    },
    {
      title: '是否生效',
      dataIndex: 'hasEffect',
      sorter: false,
      formItemProps: {
        valuePropName: 'checked',
        initialValue: false,
      }
    },
    {
      title: '开票金额',
      dataIndex: 'taxAmount',
      sorter: false,
    },
    {
      title: '税点',
      // dataIndex: 'taxRate',
      dataIndex: 'taxRate',
      valueType: 'customPercent',
      fieldProps: {
        max: 1,
        min: 0,
        placeholder: '请输入小数',
        step: 0.01
      },
      sorter: false,
    },
    // {
    //   title: '操作人',
    //   dataIndex: 'userName',
    //   editable: false,
    //   sorter: false,
    // },
    // {
    //   title: '创建时间',
    //   dataIndex: 'createTime',
    //   valueType: 'dateTime',
    //   editable: false,
    //   sorter: false,
    // },
    // {
    //   title: '更新时间',
    //   dataIndex: 'updateTime',
    //   valueType: 'dateTime',
    //   editable: false,
    //   sorter: false,
    // },
  ];

  const columnsTax = makeColumns(columnsTaxBase);

  // 表格数据
  const [data, setData] = React.useState<Record<string, any>>([]);

  const tables = [
    {column: columnsIncomeNode, name: '收款节点', dataKey: 'nodeVoList'},
    {column: columnsIncomeRecord, name: '到账记录', dataKey: 'receiveRecordVoList'},
    {column: columnsIncome, name: '确认收入', dataKey: 'confirmIncomeVoList'},
    {column: columnsPaid, name: '现金支出', dataKey: 'payVoList'},
    {column: columnsTax, name: '开票记录', dataKey: 'taxVoList'},
  ];

  // 获取节点列表，生产节点map
  const getProjectNodes = async (projectId: string) => {
    const nodesRes = await getNodes({projectId});

    const map = {};
    (nodesRes.data||[]).forEach((item: any) => {
      if (item?.id) {
        map[item.id + ''] = item.nodeName;
      }
    })

    setCurrentNodeMap(map);
  }

  React.useEffect(() => {
    getProjectNodes(id)
  }, [id])

  return (
    <PageContainer
      extra={
        accessKeySet.has('project_del') ? (
          <Popconfirm title="确定删除？" onConfirm={async() => {
            const success = await handleRemoveProject(id);
            if (success) {
              history.goBack();
            }
          }}>
            <Button type="primary">删除项目</Button>
          </Popconfirm>
        ) : null
      }
    >
      <ProTable<TableListItem, TableListPagination>
        headerTitle="项目概览"
        actionRef={actionRef}
        rowKey="id"
        search={false}
        request={projectDetail}
        params={{id}}
        postData={(d: Record<string, any>) => {
          setData(d);
          return d?.projectVo ? [d.projectVo] : [];
        }}
        columns={columns1}
        rowSelection={false}
        options={{
          fullScreen: false,
          reload: false,
          setting: false,
          density: false
        }}
        pagination={false}
        scroll={{x: 'max-content'}}
      />
      <Collapse
        bordered={false}
        defaultActiveKey={['0', '1', '2', '3', '4']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        className="site-collapse-custom-collapse"
      >
        {
          tables.map((t, i) => {
            return (
              <Collapse.Panel header={t.name} key={i} className="site-collapse-custom-panel">
                <ProTable<TableListItem, TableListPagination>
                  // headerTitle=``
                  actionRef={actionRef}
                  rowKey="id"
                  search={false}
                  // request={() => Promise.resolve(data?.[t.dataKey] || [])}
                  dataSource={data?.[t.dataKey] || []}
                  columns={t.column}
                  rowSelection={false}
                  options={{
                    fullScreen: false,
                    reload: false,
                    setting: false,
                    density: false
                  }}
                  pagination={false}
                />
              </Collapse.Panel>
            )
          })
        }
      </Collapse>
    </PageContainer>
  );
};

export default RoleList;
