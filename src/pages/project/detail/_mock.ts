// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';
import { parse } from 'url';

function projectDetail(req: Request, res: Response, u: string, b: Request) {
  const result = {
    status: 200,
    total: 5,
    success: true,
    pageSize: 1,
    data: {
      "confirmIncomeVoList": [
            {
              "completionRate": 0,
              "confirmAmount": 0,
              "confirmDate": "",
              "createTime": "",
              "dataDirector": "",
              "hasReceipt": true,
              "id": 0,
              "projectId": 0,
              "updateTime": "",
              "userId": "",
              "userName": ""
            }
        ],
        "nodeVoList": [
          {
            "closeAmount": 0,
            "completionRate": 0,
            "createTime": new Date(),
            "endDate": new Date(),
            "id": 0,
            "nodeCondition": "nodeCondition",
            "nodeName": "nodeName",
            "projectId": 0,
            "receivableAmount": 0,
            "receivedAmount": 0,
            "type": "",
            "updateTime": new Date(),
            "userName": 0
          }
        ],
        "payVoList": [
          {
            "actualAmount": 0,
            "createTime": "",
            "expectAmount": 0,
            "id": 0,
            "payDate": "",
            "plan": "",
            "projectId": 0,
            "remark": "",
            "updateTime": "",
            "userId": 0
          }
        ],
        "projectVo": {
          "closeAmount": 0,
          "confirmAmount": 0,
          "confirmContractRate": 0,
          "confirmReceivableDiffAmount": 0,
          "contractAmount": 0,
          "contractDate": new Date(),
          "contractRate": 0,
          "createTime": new Date(),
          "dataDirector": "",
          "endDate": new Date(),
          "id": 0,
          "isBadDebt": true,
          "lastFyExpectReceivedAmount": 0,
          "partnerName": "partnerName",
          "projectName": "projectName",
          "projectStatus": 0,
          "projectType": "tinei",
          "businessType": "tinei",
          "receivableAmount": 0,
          "receiveCycle": 0,
          "receiveDirector": "receiveDirector",
          "receivedAmount": 0,
          "remark": "asd",
          "startDate": new Date(),
          "taxPoint": "10",
          "taxUnreceiveAmount": 0,
          "thisFySurplusQExpectReceivedAmount": 0,
          "updateTime": new Date(),
          "userName": "userName",
          "workDay": 0
        },
        "receiveRecordVoList": [
          {
            "amount": 0,
            "createTime": new Date(),
            "honorPayType": '0',
            "honorTaxNo": "honorTaxNo",
            "honorTaxRate": 0,
            "id": 0,
            "payType": '0',
            "projectId": 0,
            "receiveDate":  new Date(),
            "receiveNodeId": 0,
            "remak": "remak",
            "taxId": 0,
            "updateTime": new Date(),
            "userId": 0
          }
        ]
    }
  }

  res.json(result);
}


export default {
  'GET /project/0': projectDetail,
};
