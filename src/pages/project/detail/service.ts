// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/**查询项目详情 */
export async function projectDetail(data?: { id?: number|string, [key: string]: any }, options?: { [key: string]: any }) {
  if (data) {
    data.currentPage = data.current;
    delete data.current;
  }
  return request<Record<string, any>>(`/project/${data?.id}`, {
    data,
    method: 'GET',
    ...(options || {}),
  });
}

/** 删除项目 */
export async function removeProject(data?: { id?: number|string, [key: string]: any }, options?: { [key: string]: any }) {
  if (data) {
    data.currentPage = data.current;
    delete data.current;
  }
  return request<Record<string, any>>(`/project/${data?.id}`, {
    data,
    method: 'DELETE',
    ...(options || {}),
  });
}
