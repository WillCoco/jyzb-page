// eslint-disable-next-line import/no-extraneous-dependencies
import type { Request, Response } from 'express';

function project(req: Request, res: Response, u: string, b: Request) {
  const result = {
    "code": 200,
    "data": "",
    "desc": ""
  }

  return res.json(result);
}

export default {
  'POST /project': project,
};
