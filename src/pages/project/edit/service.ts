// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { TableListItem } from './data';


/** 新建规则 POST /api/rule */
export async function addProject(data?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>('/project', {
    data,
    method: 'POST',
    ...(options || {}),
  });
}


/** 新建规则 POST /api/rule */
export async function editProject(data?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>('/project', {
    data,
    method: 'PUT',
    ...(options || {}),
  });
}


/** 新增节点 */
export async function addNode(data?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>('/receiveNode', {
    data,
    method: 'POST',
    ...(options || {}),
  });
}

/** 编辑节点 */
export async function editNode(data?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>('/receiveNode', {
    data,
    method: 'PUT',
    ...(options || {}),
  });
}

/** 删除节点 */
export async function delNode(params?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>(`/receiveNode/${params?.id}`, {
    method: 'DELETE',
  });
}

/** 金额转百分比 */
export async function amount2rate(params?: { [key: string]: any }, options?: { [key: string]: any }) {
  return request<TableListItem>('/receiveNode/rate', {
    params,
    method: 'GET',
    ...(options || {}),
  });
}

