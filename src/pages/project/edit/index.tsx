import { message, Popconfirm, Tag, Select } from 'antd';
import { useLocation, useModel, useParams } from 'umi';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { EditableProTable } from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import ProForm, { BetaSchemaForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { addProject, editProject as submitEditProject, addNode, delNode, editNode, amount2rate } from './service';
import { projectDetail } from '../detail/service';
import type { TableListItem, TableListPagination } from './data';
import type { DictType } from '@/types/project';
import withAccess from '@/utils/withAccess';
import makeColumns from '@/utils/columns/makeColumns';

const EditNodeBtn = withAccess('node_edit', 'a');

let keyIndex = -1;


const ProjectEdit: React.FC = () => {
  const {id: projectId} = useParams<{id: string}>()
  // 新增or编辑
  const {state: {type = 'new'} = {}} = useLocation<{type: 'edit'|'new'|'addNode'}>();

  const { initialState } = useModel('@@initialState');
  const { dicts, currentUser, receiveDirectors }: {dicts: Record<DictType, any>} = initialState || {};
  const { accessKeySet = new Set() } = currentUser || {};

  console.log(receiveDirectors, 'receiveDirectors')

  // params
  const [params, setParams] = React.useState<number>(0);
  const refreshForm = () => setParams((v: number) => v+1);

  const options = ({
    new: {
      tableName: '项目节点',
      rate: 'completionRate'
    },
    edit: {
      tableName: '项目节点',
      rate: 'completionRate'
    },
    addNode: {
      tableName: '项目节点',
      rate: 'completionRate'
    }
  })[type];

  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [currentRow, setCurrentRow] = useState<any>();

  // console.log(editableKeys, 'editableKeys')

  const formRef = useRef<ProFormInstance<any>>();

  // const deleteRowFromForm = (ref: ProFormInstance<any>, key: string) => {
  //   console.log(
  //     ref?.current?.getFieldsValue(),
  //     'asldkjhaskjdhg'
  //   )
  // }

  // // 新建时计算所有节点的
  // const allNodeAmount = () => {
  //   if (type !== 'new') {
  //     return;
  //   }
  //   const data = formRef.current?.getFieldsValue();
  //   return (data?.nodeVoList || []).reduce((prev: any, next: any) => {
  //     return parseFloat(prev?.receivableAmount||0) + parseFloat(next?.receivableAmount||0)
  //   }, 0)
  // }

  // 金额换比例
  const getRate = (v: any) => {
    const currentKey = editableKeys?.[0];
    const formValues = formRef.current?.getFieldsValue();
    const {nodeVoList} = formValues || {};

    // TODO：新建不获取
    if (type === 'new' || type === 'addNode') {
    //   const allAmount = allNodeAmount();
    //   console.log(allAmount ? (v?.amount / allAmount||0).toFixed(2) : 0, 'vvvvvvvv')
    //   const newNodeVList = nodeVoList.map((d: any) => {
    //     console.log(d?.id, currentKey, 'currentKey')
    //     if (d?.id === currentKey) {
    //       return {
    //         ...d||{},
    //         completionRate: allAmount ? (v?.amount / allAmount||0).toFixed(2) : 0
    //       };
    //     }
    //     return d;
    //   })
    // console.log(newNodeVList, 'newNodeVList')
    //   formRef.current?.setFieldsValue({nodeVoList: newNodeVList})
      return;
    }

    amount2rate({...v, nodeId: currentRow?.id ? currentRow.id : undefined})
      .then(r => {
        console.log(r, 'rrrrrr', editableKeys)
        // setRate(r?.data)
        const newNodeVList = nodeVoList.map((d: any) => {
          if (d.id === currentKey) {
            return {
              ...d||{},
              completionRate: r?.data || 0
            };
          }
          return d;
        })
        formRef.current?.setFieldsValue({nodeVoList: newNodeVList})
      })
      .catch(console.error)
  }

  const columnsBase: ProColumns<TableListItem>[] = [
    {
      title: '节点名称',
      dataIndex: 'nodeName',
    },
    {
      title: '计划收款日期',
      dataIndex: 'endDate',
      valueType: 'date'
    },
    {
      title: '收款条件',
      dataIndex: 'nodeCondition',
    },
    {
      title: '款项类型',
      dataIndex: 'type',
      valueEnum: dicts.NODE_TYPE,
    },
    {
      title: '计划收款进度',
      dataIndex: 'completionRate',
      // hideInForm: true,
      editable: false,
      valueType: 'customPercent',
      fieldProps: {
        max: 1,
        min: 0,
        placeholder: '请输入小数',
        step: 0.01
      }
    },
    {
      title: '计划收款金额',
      dataIndex: 'receivableAmount',
      fieldProps: {
        min: 0,
        onChange: (e: any) => {
          getRate({
            amount: e.target.value,
            projectId,
          })
        },
        onBlur: (e: any) => {
        }
      }
    },
    {
      title: '实际收款进度',
      dataIndex: 'actualRate',
      // hideInForm: true,
      sorter: true,

      editable: false,
      valueType: 'customPercent',
      fieldProps: {
        max: 1,
        min: 0,
        placeholder: '请输入小数',
        step: 0.01
      },
    },
    {
      title: '实际可收金额',
      dataIndex: 'closeAmount',
      fieldProps: {
        min: 0,
      }
    },
    {
      title: '已收金额',
      dataIndex: 'receivedAmount',
      editable: false,
    },
    // {
    //   title: '待收金额',
    //   dataIndex: '3',// TODO:缺少字段
    // },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (text, record) => [
        <EditNodeBtn
          key="edit"
          onClick={() => {
            setEditableRowKeys([record?.id]);
            setCurrentRow(record);
          }}
        >
          编辑
        </EditNodeBtn>,
      ],
    },
  ];

  const columns = makeColumns(columnsBase, {
    required: ['nodeName', 'endDate', 'rate', 'receivableAmount', 'type']
  })

  // 表单
  const formColumns = [
    {
      title: '项目信息',
      valueType: 'group',
      columns: [
        {
          title: '项目名称',
          dataIndex: ['projectVo', 'projectName'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '甲方名称',
          dataIndex:  ['projectVo', 'partnerName'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '项目类型',
          dataIndex:  ['projectVo', 'projectType'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          valueType: 'select',
          valueEnum: dicts.PROJECT_TYPE,
          width: 's',
        },
        {
          title: '合同金额（万）',
          dataIndex:  ['projectVo', 'contractAmount'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '税率',
          dataIndex:  ['projectVo', 'taxPoint'],
          fieldProps: {
            max: 1,
            min: 0,
            placeholder: '请输入小数',
            step: 0.01
          },
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
          valueType: 'customPercent'
        },
        {
          title: '合同签订时间',
          dataIndex:  ['projectVo', 'contractDate'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
          valueType: 'date'
        },
        {
          title: '结算金额(万)',
          dataIndex:  ['projectVo', 'closeAmount'],
          formItemProps: {
          },
          width: 's',
          valueType: 'amount'
        },
        {
          title: '项目状态',
          dataIndex:  ['projectVo', 'projectStatus'],
          valueEnum: dicts.PROJECT_STATUS,
          valueType: 'select',
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '业务类型',
          dataIndex:  ['projectVo', 'businessType'],
          valueType: 'select',
          valueEnum: dicts.BUSINESS_TYPE,
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '开工时间',
          dataIndex:  ['projectVo', 'startDate'],
          valueType: 'date',
          hideInSearch: true,
          width: 's',
        },
        {
          title: '完工时间',
          dataIndex:  ['projectVo', 'endDate'],
          valueType: 'date',
          hideInSearch: true,
          formItemProps: {
          },
          width: 's',
        },
        {
          title: '备注',
          dataIndex:  ['projectVo', 'remark'],
          formItemProps: {},
          width: 's',
        },
        {
          title: '收款负责人',
          valueType: 'select',
          dataIndex:  ['projectVo', 'receiveDirectorIdList'],
          formItemProps: {
          },
          width: 's',
          fieldProps: {
            options: receiveDirectors.map((v: any) => ({value: v?.receiveDirectorId, label: v?.receiveDirectorName})),
            maxTagCount: 'responsive',
          },
          renderFormItem: (value, props) => {
            return (
              <Select
                {...props?.fieldProps}
                maxTagCount='responsive'
                size="small"
                mode="multiple"
              />
            )
          },
        },
        {
          title: '项目经理',
          dataIndex:  ['projectVo', 'dataDirector'],
          formItemProps: {
            rules: [
              {
                required: true,
                message: '此项为必填项',
              },
            ],
          },
          width: 's',
        },
        {
          title: '结算时间',
          dataIndex:  ['projectVo', 'closeDate'],
          valueType: 'date',
          width: 's',
        },
        {
          title: '是否坏账',
          dataIndex:  ['projectVo', 'isBadDebt'],
          valueType: 'switch',
          formItemProps: {},
          width: 's',
        },
      ]
    }
  ]

  // 初始值
  const getInitialValues = async () => {
    if (!projectId) return Promise.resolve({});
    try {
      const r = await projectDetail({id: projectId});
      return r?.data;
    } catch (err) {
      console.error(err)
    }
  };

  // 新建的提交
  const newProject = (values: any) => {
    const {nodeVoList, projectVo} = values || {};
    const receiveNodeAddVoList = (nodeVoList||[]).map((item: any) => {
      if (item?.id < 0) {
        const newItem = {
          ...item||{},
          rate: item?.completionRate
        };
        delete newItem.id;
        if (newItem.completionRate) {
          delete newItem.completionRate;
        }
        return newItem;
      }
    })
    addProject({
      ...projectVo,
      receiveNodeAddVoList
    })
      .then((r: any) => {
        if (r?.code === 200) {
          message.success('新建项目成功');
          history.back();
        }
      })
      .catch(() => {
      })
      .finally(() => {
      })
  }

  // 编辑项目
  const editProject = (values: any) => {
    const {projectVo} = values || {};
    submitEditProject({
      ...projectVo||{},
      id: projectId
    })
      .then((r: any) => {
        if (r?.code === 200) {
          message.success('修改成功');
          history.back();
        }
      })
      .catch(() => {
      })
      .finally(() => {
      })
  }

  // 新增节点
  const addNodes = (row: any) => {
    addNode({
      ...row,
      projectId
    })
      .then((r: any) => {
        if (r?.code === 200) {
          message.success('新增成功');
        }
      })
      .catch(() => {
      })
      .finally(() => {
        refreshForm();
      })
  }

  // 编辑节点
  const editNodes = (row: any) => {
    editNode(row)
      .then((r: any) => {
        if (r?.code === 200) {
          message.success('修改成功');
        }
      })
      .catch(() => {
      })
      .finally(() => {
        refreshForm();
      })
  }

  // 点击提交
  const handleFinish = () => {
    if (type === 'new') {
      return newProject;
    }
    if (type === 'edit') {
      return editProject;
    }
  }

  // 点击保存
  const onSave = (key, row) => {
    if (type !== 'new') {
      // 添加
      if (key < 0) {
        addNodes(row);
        return;
      }
      // 编辑
      editNodes(row);
    }
  }
  // 点击删除
  const onDelete = (key, row) => {
    // 本地删除
    if (key < 0) {
      refreshForm();
      return;
    }
    // 远程删除
    delNode(row)
      .then((r: any) => {
        if (r?.code === 200) {
          message.success('删除成功');
        }
      })
      .catch()
      .finally(() => {
        refreshForm();
      })
  }

  return (
    <PageContainer>
      <ProCard>
        <ProForm
          formRef={formRef}
          request={type !== 'new' ? getInitialValues : getInitialValues}
          params={{params}}
          submitter={{
            render: (props, dom: React.ReactNode[])=> {
              // 只有新建可重置
              if (type === 'edit') {
                return dom[1];
              }
              if (type === 'new') {
                return dom;
              }
              return null
            }
          }}
          onFinish={handleFinish()}
        >
          {type !== 'addNode' ? (
            <BetaSchemaForm
              layout="horizontal"
              layoutType="Embed"
              columns={formColumns}
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 10 }}
              labelAlign="right"
              onFinish={async (values) => {
                console.log(values);
              }}
            />) : null
          }
          {type !== 'edit' ? (
            <ProForm.Item
              label={options.tableName}
              name="nodeVoList"
              trigger="onValuesChange"
            >
              <EditableProTable<any, TableListPagination>
                headerTitle="收款节点"
                rowKey="id"
                search={false}
                toolBarRender={false}
                columns={columns}
                scroll={{x: 'max-content'}}
                pagination={false}
                editable={{
                  type: 'single',
                  editableKeys,
                  onChange: setEditableRowKeys,
                  onSave,
                  onCancel: () => setCurrentRow(undefined),
                  onDelete
                }}
                recordCreatorProps={{
                  record: () => {
                    return {
                      id: keyIndex--,
                      completionRate: ''
                    };
                  },
                  creatorButtonText: '新增节点',
                  onClick: () => setCurrentRow({
                    id: keyIndex,
                    completionRate: ''
                  }),
                  ...(accessKeySet.has('node_add') ? {} : {style: {display: 'none'}})
                }}
              />
            </ProForm.Item>
          ) : null}

        </ProForm>
      </ProCard>
    </PageContainer>
  );
};

export default ProjectEdit;
