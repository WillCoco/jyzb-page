import type { FC } from 'react';
import React, { Suspense, useState } from 'react';
import { Col, Dropdown, Card, Row, DatePicker } from 'antd';
import { GridContent, PageContainer } from '@ant-design/pro-layout';
import { Line, Pie } from '@ant-design/charts';
import IntroduceRow from './components/IntroduceRow';
import Moment from 'moment';

import { getRTData, getMonthCash, getRTReceived, getMonthCompleted, getConfirm, getQuarterConfirm, getYearConfirm } from './service';
import PageLoading from './components/PageLoading';
import MsgList from './components/MsgList';
import type { AnalysisData } from './data.d';
import styles from './style.less';


type AnalysisProps = {
  dashboardAndanalysis: AnalysisData;
  loading: boolean;
};

const Analysis: FC<AnalysisProps> = () => {
  const currentMoment = React.useMemo(() => Moment(), []);
  const [year, setYear] = useState<any>(currentMoment.format('YYYY'));

  const [loading, setLoading] = useState<any>(false);
  // 实时合计
  const [RTData, setRTData] = useState<any>({});

  // 实时收款比例(返回data为已收比例)
  const [RTReceived, setRTReceived] = useState<any>([]);

  // 当年每月现金流
  const [monthCash, setMonthCash] = useState<any>([]);
  // 当年每月完成率
  const [completedList, setCompletedList] = useState<any>([]);

  // 当年确认
  const [confirmData, setConfirm] = useState<any>([]);
  // 历年
  const [yearData, setYearData] = useState<any>([]);
  // 当年季度
  const [quarterConfirm, setQuarterConfirm] = useState<any>([]);

  React.useEffect(() => {
    // 合计
    getRTData()
      .then(r => {
        setRTData(r?.data || {});
      })

    // 当年每月现金流
    getMonthCash({year})
      .then(r => {
        setMonthCash(r?.data || []);
      })

    // 实时收款比例(返回data为已收比例)
    getRTReceived()
      .then(r => {
        const received = parseFloat(((r?.data || 0) * 100).toFixed(2));
        setRTReceived([{value: received, name: '已收'}, {value: parseFloat((100 - received).toFixed(2)), name: '未收'}]);
      })

    // 历年确认收入（万元）
    getYearConfirm()
      .then(r => {
        setYearData(r?.data || []);
      })
  }, [])

  React.useEffect(() => {
    // 当年每月现金流
    getMonthCash({year})
      .then(r => {
        setMonthCash(r?.data || []);
      })
    // 当年每月应收完成率
    getMonthCompleted({year})
      .then(r => {
        setCompletedList(r?.data || []);
      })
    // 当年确认收入完成率
    getConfirm({year})
      .then(r => {
        const confirm = parseFloat(((r?.data || 0) * 100).toFixed(2));
        setConfirm([{value: confirm, name: '已完成'}, {value: parseFloat((100 - confirm).toFixed(2)), name: '未完成'}]);
      })
    // 当年每季度确认收入（万元）
    getQuarterConfirm({year})
      .then(r => {
        setQuarterConfirm(r?.data||[]);
      })
  }, [year])

  return (
    <PageContainer
      extra={[
        <DatePicker
          key="year"
          picker="year"
          defaultValue={currentMoment}
          onChange={(m, s) => setYear(s)}
        />
      ]}
    >
      <GridContent>
        <>
          <Suspense fallback={<PageLoading />}>
            <IntroduceRow loading={loading} visitData={RTData || []} />
          </Suspense>

          <Suspense fallback={null}>

          </Suspense>

          <Card>
            <Row
              gutter={24}
              style={{
                marginTop: 24,
              }}
            >
              <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Line
                    title={{
                      text: '本年每月现金流（万元）',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    data={monthCash}
                    responsive
                    xField="month"
                    yField="amount"
                    seriesField="amount"
                    legend={{
                      visible: false
                      // position: 'top-center',
                    }}
                  />
                </Suspense>
              </Col>
              <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <MsgList />
                </Suspense>
              </Col>
            </Row>
            {/* 第二行 */}
            <Row
              gutter={24}
              style={{
                marginTop: 24,
              }}
            >
              <Col xl={8} lg={24} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Pie
                    title={{
                      text: '实时收款比例',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    angleField="value"
                    data={RTReceived}
                    meta={{
                      value: {
                        formatter: v => `${v}%`
                      }
                    }}
                    responsive
                    colorField="name"
                    label={{
                      type: 'outer',
                      visible: true,
                    }}
                  />
                </Suspense>
              </Col>
              <Col xl={16} lg={24} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Line
                    title={{
                      text: '本年应收账款完成率',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    data={completedList}
                    meta={{
                      rate: {
                        formatter: v => `${(v*100).toFixed(0)}%`
                      }
                    }}
                    responsive
                    xField="month"
                    yField="rate"
                    seriesField="rate"
                    legend={{
                      visible: false,
                    }}
                  />
                </Suspense>
              </Col>
            </Row>
            {/* 第三行 */}
            <Row
              gutter={24}
              style={{
                marginTop: 24,
              }}
            >
              <Col xl={8} lg={8} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Pie
                    title={{
                      text: '确认收入完成率',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    angleField="value"
                    data={confirmData}
                    meta={{
                      value: {
                        formatter: v => `${v}%`
                      }
                    }}
                    responsive
                    colorField="name"
                    label={{
                      type: 'outer',
                      visible: true,
                    }}
                  />
                </Suspense>
              </Col>
              <Col xl={8} lg={8} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Line
                    title={{
                      text: '本年每季度确认收入（万元）',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    data={quarterConfirm}
                    responsive
                    xField="q"
                    yField="amount"
                    seriesField="amount"
                    legend={{
                      visible: false
                    }}
                  />
                </Suspense>
              </Col>
              <Col xl={8} lg={8} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <Line
                    title={{
                      text: '历年确认收入（万元）',
                      visible: true
                    }}
                    forceFit
                    height={300}
                    data={yearData}
                    responsive
                    xField="year"
                    yField="amount"
                    seriesField="amount"
                    legend={{
                      visible: false
                    }}
                  />
                </Suspense>
              </Col>
            </Row>
          </Card>
        </>
      </GridContent>
    </PageContainer>
  );
};

export default Analysis;
