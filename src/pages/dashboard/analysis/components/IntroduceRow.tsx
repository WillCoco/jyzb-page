import { Col, Row, Tooltip } from 'antd';

import numeral from 'numeral';
import { ChartCard, Field } from './Charts';
import type { DataItem } from '../data.d';

const topColResponsiveProps = {
  // xs: 24,
  // sm: 8,
  // md: 8,
  // lg: 8,
  // xl: 4,
  flex: 1,
  style: { marginBottom: 24 },
};

const dataList = [{
  name: '实时应收合计',
  key: 'receivableAmount',
  postfix: '万元'
}, {
  name: '实时确认收入合计',
  key: 'confirmAmount',
  postfix: '万元'
}, {
  name: '实时收入合计',
  key: 'receivedAmount',
  postfix: '万元'
}, {
  name: '实时支出合计',
  key: 'payAmount',
  postfix: '万元'
}, {
  name: '实时毛利率',
  key: 'grossRate',
  postfix: '%',
  format: (v: number) => ((v * 100) || 0).toFixed(2)
}, ]

const IntroduceRow = ({ loading, visitData }: { loading: boolean; visitData: any }) => (

  <Row gutter={24}>
    {
      dataList.map((d) => {
        const v = d.format ? d.format(visitData[d.key]) : visitData[d.key];
        const finalV = v  && `${v}`.replace(/(\d{1,3})(?=(\d{3})+(\.\d*)?$)/g, '$&,')
        return (
          <Col {...topColResponsiveProps} key={d.key}>
            <ChartCard
              bordered={false}
              loading={loading}
              title={d.name}
              total={(
                <div
                  style={(d.key === 'grossRate' && visitData.grossRateWarn) ? {color: 'red'} : {}}
                >
                    {`${finalV||'-'}${d.postfix}`}
                </div>
              )}
              contentHeight={46}
            />
          </Col>
        )
      })
    }
  </Row>
);

export default IntroduceRow;
