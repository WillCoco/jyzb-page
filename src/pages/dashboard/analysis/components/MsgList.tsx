import React from 'react';
import { Card, Typography, notification } from 'antd';
import { getMsgs} from '../service';
import ProList from '@ant-design/pro-list';
import { ReactComponent as Gou } from '@/assets/gou.svg';
import { ReactComponent as FillGou } from '@/assets/fill-gou.svg';
import { getMsgDetail } from '../service';
import styles from './style.less';


const MsgList = ({
}: {
}) => {

  const [params, setParams] = React.useState<number>(1);

  const read = async(id: string) => {
    try {
      notification.destroy();
      const res = await getMsgDetail(id);
      setParams(v => ++v);
      const {data} = res;
      notification.open({
        message: data.createTime,
        duration: 0,
        description: data.content,
      });
    } catch {}
  }

  return (
    <div className={styles['msg-list']}>
      <Card bordered={false} style={{  }}>
        <ProList
          request={getMsgs}
          pagination={false}
          params={{params}}
          metas={{
            description: {
              dataIndex: 'content',
              render: (t) => {
                return <Typography.Paragraph ellipsis style={{margin: 0}}>{t}</Typography.Paragraph>;
              },
            },
            actions: {
              dataIndex: 'hasRead',
              render: (t, r) => {
                return !r.hasRead ? <Gou className={styles.icon} /> : <FillGou className={styles.icon} />
              },
            },
          }}
          onItem={r => {
            return {
              onClick: () => {read(r.id)}, // 点击行
            };
          }}
        />
      </Card>
    </div>
  )
};

export default MsgList;
