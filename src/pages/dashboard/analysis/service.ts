import { request } from 'umi';
import type { AnalysisData } from './data';

// 合计
export async function getRTData(): Promise<{ data: any }> {
  return request('/board/rt');
}

// 实时收款比例(返回data为已收比例)
export async function getRTReceived(): Promise<{ data: any }> {
  return request('/board/received/rate');
}
// 每月现金流
export async function getMonthCash(params: any): Promise<{ data: any }> {
  return request('/board/month/cash/amount', {
    params
  });
}
// 每月应收完成率
export async function getMonthCompleted(params: any): Promise<{ data: any }> {
  return request('/board/month/receivable/rate', {
    params
  });
}

// 确认完成率
export async function getConfirm(params: any): Promise<{ data: any }> {
  return request('/board/confirm/rate', {
    params
  });
}

// 每季度确认收入
export async function getQuarterConfirm(params: any): Promise<{ data: any }> {
  return request('/board/q/confirm/amount', {
    params
  });
}

// 历年确认收入
export async function getYearConfirm(): Promise<{ data: any }> {
  return request('/board/year/confirm/amount');
}

// 消息列表
export async function getMsgs(): Promise<{ data: any }> {
  return request('/board/msg');
}

// 读取消息详情
export async function getMsgDetail(id: string): Promise<{ data: any }> {
  return request(`/board/msg/${id}`);
}
