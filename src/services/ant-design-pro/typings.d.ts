// @ts-ignore
/* eslint-disable */

declare namespace API {
  type BtnAuthType = "project_add" | "project_import" | "project_edit" | "project_del" | "confirmIncome_add" | "confirmIncome_edit" | "confirmIncome_del" | "pay_add" | "pay_edit" | "pay_del" | "plan_edit" | "node_add" | "node_edit" | "node_del" | "record_add" | "record_edit" | "record_del" | "role_add" | "role_edit" | "role_del" | "tax_add" | "tax_edit" | "tax_del" | "user_add" | "user_edit" | "user_del" | "user_reset";
  type CurrentUser = {
    username?: string;
    realName?: string;
    btnAuthList: Record<BtnAuthType, string>[];
    menuAuthList: Record<string, string>[];
    accessKeySet: string[]; // 权限key组成的Set，便于权限key提取
    avatar?: string;
    userid?: string;
    email?: string;
    signature?: string;
    title?: string;
    group?: string;
    tags?: { key?: string; label?: string }[];
    notifyCount?: number;
    unreadCount?: number;
    country?: string;
    access?: string;
    geographic?: {
      province?: { label?: string; key?: string };
      city?: { label?: string; key?: string };
    };
    address?: string;
    phone?: string;
  };

  type LoginResult = {
    code?: number;
    type?: string;
    currentAuthority?: string;
  };

  type PageParams = {
    current?: number;
    pageSize?: number;
  };

  type RuleListItem = {
    key?: number;
    disabled?: boolean;
    href?: string;
    avatar?: string;
    name?: string;
    owner?: string;
    desc?: string;
    callNo?: number;
    status?: number;
    updatedAt?: string;
    createdAt?: string;
    progress?: number;
  };

  type RuleList = {
    data?: RuleListItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type FakeCaptcha = {
    code?: number;
    status?: string;
  };

  type LoginParams = {
    username?: string;
    password?: string;
    autoLogin?: boolean;
    type?: string;
  };

  type ErrorResponse = {
    /** 业务约定的错误码 */
    errorCode: string;
    /** 业务上的错误信息 */
    errorMessage?: string;
    /** 业务上的请求是否成功 */
    success?: boolean;
  };

  type NoticeIconList = {
    data?: NoticeIconItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type NoticeIconItemType = 'notification' | 'message' | 'event';

  type NoticeIconItem = {
    id?: string;
    extra?: string;
    key?: string;
    read?: boolean;
    avatar?: string;
    title?: string;
    status?: string;
    datetime?: string;
    description?: string;
    type?: NoticeIconItemType;
  };
}
