// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 查询用户信息*/
export async function queryCurrentUser(options?: { [key: string]: any }) {
  return request<{
    data: any;
  }>(`/user/${options.id}`, {
    method: 'GET',
    ...(options || {}),
  });
}

/** 获取当前的用户菜单列表 GET /api/currentUser */
export async function queryMenus(options?: { [key: string]: any }) {
  return request<{
    data: any;
  }>('/api/menus', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 获取项目的枚举字典信息 GET /api/currentUser */
export async function queryDicts(type: string) {
  return request<{
    data: any;
  }>(`/dict/${type}`, {
    method: 'GET',
  });
}

/** 获取项目的权限字典信息 GET /role/{id}/auth/{type} */
export async function queryAccessDicts(id: number, type: string) {
  return request<{
    data: any;
  }>(`/role/${id}/auth/${type}`, {
    method: 'GET',
  });
}

/* 导出 */
export async function getExportExcel(url: string, params: any) {
  return request<{
    data: any;
  }>(url, {
    method: 'GET',
    params,
    responseType: 'blob',
    getResponse: true
  });
}

/**查询项目负责人列表 */
export async function getReceiveDirectors() {
  return request<Record<string, any>>('/project/ReceiveDirector', {
    method: 'GET',
  });
}
