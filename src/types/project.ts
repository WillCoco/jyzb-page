// 项目状态
export enum ProjectStatusEnum {
  '正常',
  '终止',
  '完成'
}

// 项目类型
export enum ProjectTypeEnum {
  '体内',
  '体外',
}

// 项目业务类型
export enum BusinessTypeEnum {
  '业务1',
  '业务2',
}


// 收款节点-款项类型
export enum AmountTypeEnum {
  '款项1',
  '款项2',
}


// 字典
export type DictType =
  'PROJECT_TYPE'| //项目类型
  'BUSINESS_TYPE'| //业务类型
  'PROJECT_STATUS'| //项目状态
  'PAY_TYPE'| //支付方式
  'HONOR_PAY_TYPE'| //承兑类型
  'NODE_TYPE'| //FY收款节点
  'ROLES'| // 角色
  'MONTH'; // 收款计划可选月
