import React from 'react';
import { Popover } from 'antd';
import withAccess from '@/utils/withAccess';
import { EditOutlined } from '@ant-design/icons';

function PopoverIcon(props: {
  icon: any;
  children: React.ReactNode;
  popoverProps: any;
  accessKey: API.BtnAuthType;
  onClick: () => void;
}) {
  const AccessA = withAccess(props.accessKey, 'a');

  return (
    <Popover
      // getPopupContainer={(node) => node}
      {...props.popoverProps}
    >
      <AccessA onClick={props.onClick}>{props.icon || <EditOutlined/>}</AccessA>
    </Popover>
  );
}

export default PopoverIcon;
