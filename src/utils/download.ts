async function downloadWithBlob(res: any) {
  const {data: blob, response} = res || {};
  if (!response) return;
  const contentDisposition = response.headers.get('content-disposition');
  const filenameUrl = contentDisposition.match(/.*?;filename=(.*)/)?.[1];
  const filename = decodeURIComponent(filenameUrl) || '表格';
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.href = url;
  a.download = filename;
  a.click();
  window.URL.revokeObjectURL(url);
}

export default downloadWithBlob;
