import React from "react"

export default (list: any) => {
  const newList = list.map((item: any) => {
    if (item.access) {
      return item.dom
    }
    return <a style={{pointerEvents: 'none'}}>{item.dom}</a>
  });

  console.log(newList, 'newList');

  return newList;
};

