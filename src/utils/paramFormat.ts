const sortMap = {
  'ascend': 'asc',
  'descend': 'desc'
}

export default (options: any) => {
  if (!options || !options.params) return options;
  const {params} = options;
  if (!params) return params;
  // 处理分页
  if (params.current) {
    options.params.currentPage = params.current;
    delete options.params.current;
  }
  // 处理排序
  if (options.otherParams) {
    Object.entries(options.otherParams).forEach(([name, sort]) => {
      if (['ascend', 'descend'].includes(sort as string)) {
        options.params.orderByType = sortMap[sort as keyof typeof sortMap];
        options.params.sortField = name;
      }
    })
  }
  return options;
}
