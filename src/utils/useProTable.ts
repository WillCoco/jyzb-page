import React from 'react';


export default () => {
  const ref = React.useRef<any>();

  // sorter
  const [sorter, setSorter] = React.useState({});
  const [orderByType, setOrderByType] = React.useState();
  const [sortField, setSortField] = React.useState();

  // filter
  const [filter, setFilter] = React.useState({});

  // 获取表格的sorter、filter信息
  const onChange = (_page: any, _filter: any, _sorter: any) => {
    console.log(_filter, '_filter')
    console.log(_sorter, '_sorter')
    const sorterResult = _sorter;
    if (sorterResult?.field) {
      setSortField(_sorter?.field);
      setOrderByType(_sorter?.order);
    }
  }

  return {
    onChange,
    sorter,
    filter,
    orderByType,
    sortField,
    ref
  }
}
