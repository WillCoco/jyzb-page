import {useModel} from 'umi';

type List = [API.BtnAuthType, (...p: any) => JSX.Element][]

function useAccess<T>(accessName: API.BtnAuthType|List, Component?: (...p: any) => JSX.Element): (
  T extends [] ? (((...p: any) => JSX.Element)|(() => null))[] : ((...p: any) => JSX.Element)|(() => null)
) {
  const { initialState } = useModel('@@initialState');
  const { currentUser }: {} = initialState || {};
  const { accessKeySet = new Set() } = currentUser || {};

  if (typeof accessName === 'string') {
    console.log( accessKeySet.has(accessName), accessName, 'ddddd')
    return (
      accessKeySet.has(accessName) ? Component : () => null
    );
  }

  const list = accessName;
  return list.map(([access, Comp]) => accessKeySet.has(access) ? Comp : () => null)
}

export default useAccess;
