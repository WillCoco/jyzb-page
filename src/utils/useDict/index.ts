/* 获取字段枚举 */
import React from 'react';
import {queryDicts} from '@/services/v1/api';
import {roleList} from '@/pages/access/role/service';
import type { DictType } from '@/types/project';

export default (types: DictType[]) => {
  const [state, setState] = React.useState<Record<string, any>>();

  const getDict = async () => {
    let dictsRes: any[];
    try {
      dictsRes = await Promise.all(
       types.map(t => queryDicts(t))
      )
    } catch (err) {
      dictsRes = []
    }

    const res = {} as Record<DictType, any>;
    types.forEach((t, i) => {
      res[t] = dictsRes[i]?.data
    })

    try {
      const r = await roleList();
      res.ROLES = {};
      (r?.data||[]).forEach((item: any) => {
        if (item?.id) {
          res.ROLES[item.id + ''] = item.roleName
        }
      })
    } catch (err) {}
    return res;
  }

  React.useEffect(() => {
    (async () => {
      const dicts = await getDict()
      setState(dicts);
    })()
  }, [])

  return state;
}
