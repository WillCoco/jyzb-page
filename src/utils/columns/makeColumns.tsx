import React from 'react';
import { Link, useModel } from 'umi';
import { Input, Select, Switch, InputNumber, DatePicker } from 'antd';
import type { DictType } from '@/types/project';
import moment from 'moment'
import isNil from 'lodash/isNil';

export const booleanMap = new Map(
  [
    [true, '是'],
    [false, '否']
  ]
)

export const amountValueType = {
  render: (text) => <span>{text}(万)</span>,
  renderFormItem: (text, props) => {
    return (
    <Input placeholder="请输入" {...props?.fieldProps} />
  )}
}
export const customPercentValueType = {
  render: (dataIndex: string) =>
    (dom, record) => {
      const text = record?.[dataIndex];
      if (isNil(text)) return <span>-</span>;
      return (
        <span>{((text||0)*100)?.toFixed(2)}%</span>
      );
  },
  renderFormItem: (text, props) => {
    return (
      <InputNumber
        max={1}
        min={0}
        placeholder="请输入小数"
        // {...props?.fieldProps} /* value={value} */
      />
    )
  }
}

export default (columns: Record<string, any>[]|undefined|null, ops?: {
  search?: string[]; // 若提供只能搜索这些
  edit?: string[]; // 若提供只能编辑这些
  required?: string[]; // 编辑项中是否必须
  hideInForm?: string[];
  hideInTable?: string[];
  disableDefault?: boolean;
  autoValueType?: boolean;
  actionsAccess?: boolean; // 最后一项编辑的权限
}) => {

  const { initialState } = useModel('@@initialState');
  const { dicts }: {dicts: Record<DictType, any>} = initialState || {};

  const [selectedTime, setSelectedTime] = React.useState<string>();

  const options = {
    disableDefault: true,
    autoValueType: true,
    ...ops||{}
  }
  if (!columns) return columns;
  let sColumns = columns.map((oc: any) => {
    if (!oc?.dataIndex) return oc;

    const c = {...oc||{}};

    // 宽度
    if (isNil(c.width)) {
      c.width = 120;
    }
    // 搜索
    if (options.search) {
      if (
        !options.search.includes(c.dataIndex) ||
        !c.dataIndex
      ) {
        c.hideInSearch = true;
      }
    }
    //  form隐藏
    if (options.hideInForm) {
      if (options.hideInForm.includes(c.dataIndex)) {
        c.hideInForm = true;
      }
    }
    // 表格隐藏
    if (options.hideInTable) {
      if (options.hideInTable.includes(c.dataIndex)) {
        c.hideInTable = true;
      }
    }
    // 可编辑
    if (options.edit && !options.edit.includes(c.dataIndex)) {
      c.editable = false;
    }
    // 为必须的项添加rule
    if (options.required && options.required.includes(c.dataIndex) && !c?.formItemProps?.rules) {
      if (!c.formItemProps) {
        c.formItemProps = {}
      }
      c.formItemProps.rules = [
        {
          required: true,
          message: '此项为必填项',
        },
      ];
    }
    // 排序
    if (c.dataIndex && c.valueType !== 'option' && isNil(c.sorter)) {
      c.sorter = true;
    }

    // 不可编辑项目 操作人、创建时间、更新时间
    if (options.disableDefault && ['updateTime', 'createTime', 'userName'].includes(c.dataIndex)) {
      c.editable = false;
      // c.width = 180;
    }

    // 没有的自动加valueType
    // 时间类型
    if (options.autoValueType && (
        (c.dataIndex||'')?.endsWith?.('Date')
      ) && !c.valueType
    ) {
      c.valueType = 'date';
    }
    if (options.autoValueType && (
        (c.dataIndex||'')?.endsWith?.('Time')
      ) && !c.valueType
    ) {
      c.valueType = 'dateTime';
    }
    // 金钱类型
    if (options.autoValueType && (
      (c.dataIndex||'')?.toLowerCase?.()?.endsWith('amount')
    )) {
      c.valueType = 'amount';
    }
    // 布尔类型
    if (options.autoValueType) {
      if ((
        (c.dataIndex||'')?.toLowerCase?.()?.startsWith('is')
      )) {
        c.valueType = 'select';
        c.valueEnum = booleanMap;
      }

      // 是否生效
      if ((['hasEffect', 'hasReceipt'].includes(c.dataIndex))) {
        c.valueType = 'switch';
      }
    }

    // 项目名
    if ((c.dataIndex === 'projectName')) {
      c.render = (t: any, r: any) => (
        <Link className="project-link" to={{
          pathname: `/project/list/detail/${r.id}`,
        }}>
          {t}
        </Link>)
    }

    // 项目状态
    if ((c.dataIndex === 'projectStatus')) {
      c.valueType = 'select';
      c.valueEnum = dicts.PROJECT_STATUS;
    }

    // 项目状态
    if ((c.dataIndex === 'projectType')) {
      c.valueType = 'select';
      c.valueEnum = dicts.PROJECT_TYPE;
    }

    // 业务类型
    if ((c.dataIndex === 'businessType')) {
      c.valueType = 'select';
      c.valueEnum = dicts.BUSINESS_TYPE;
    }

    // 支付类型
    if ((c.dataIndex === 'payType')) {
      c.valueType = 'select';
      c.valueEnum = dicts.PAY_TYPE;
    }

    // 支付类型
    if ((c.dataIndex === 'payType')) {
      c.valueType = 'select';
      c.valueEnum = dicts.PAY_TYPE;
    }

    // 到账类型
    if ((c.dataIndex === 'honorPayType')) {
      c.valueType = 'select';
      c.valueEnum = dicts.HONOR_PAY_TYPE;
    }

    // 加省略
    if (c.valueType !== 'amount') {
      c.ellipsis = true;
    }

    // xxx: 代替valueTypeMap实现
    if (c.valueType === 'amount') {
      if (!c.render) {
        c.render = amountValueType.render;
      }
      if (!c.renderFormItem) {
        c.renderFormItem = amountValueType.renderFormItem;
      }
    }

    if (c.valueType === 'switch') {
      if (!c.render) {
        c.render = (text, record, _, action) => {
          return (
            <span>{record?.[c.dataIndex] ? '是': '否'}</span>
          );
        }
      }

    //   if (!c.renderFormItem) {
    //     c.renderFormItem = (text, props, ...p) => {
    //       console.log(props?.record, props?.record?.[text?.dataIndex], '11111111', ...p)
    //       return (
    //         <Switch
    //           // defaultChecked={props?.record?.[text?.dataIndex] || false}
    //           // {...props?.fieldProps} /* value={value} */
    //           size="small"
    //         />
    //       )
    //     }
    //   }
    }

    if (c.valueType === 'customPercent') {
      if (!c.render) {
        c.render = customPercentValueType.render(c.dataIndex);
      }
      if (!c.renderFormItem) {
        c.renderFormItem = customPercentValueType.renderFormItem;
      }
    }

    if (c.valueType === 'sameMonthDateTime') {
      if (!c.render) {
        c.render = (dom) => {
          const text = dom.props.text;
          return (
            <span>{moment(text?.[0]).format('YYYY-MM-DD')} 到 {moment(text?.[1]).format('YYYY-MM-DD')}</span>
          )
        };
      }

      if (!c.renderFormItem) {
        c.renderFormItem = (text, props) => {
          const currentTime = moment();
          const onCalendarChange = (times: string[]) => {
            const [t1, t2] = times || [];
            setSelectedTime(t1 || t2);
          }

          const isDisabledDate = (date: any) => {
            if (!selectedTime) return false;
            let disabled;
            try {
              disabled = !moment(date).isSame(currentTime, 'month');
            } catch {
            }
            return disabled;
          }
          return <DatePicker.RangePicker {...props?.fieldProps} value={moment(props?.fieldProps?.value)} onCalendarChange={onCalendarChange} disabledDate={isDisabledDate} />
        }
      }
    }

    return c;
  })

  // 操作权限
  if (isNil(options.actionsAccess)) {
    if (options.actionsAccess === false) {
      sColumns = sColumns.splice(-1)
    }
  }
  return sColumns;
}
