import React from 'react';
import {useModel} from 'umi';

function withAccess<T>(accessName: API.BtnAuthType, Component: any, type: 'disable' | 'hide' = 'disable') {

  return (props: T) => {
    const { initialState } = useModel('@@initialState');
    const { currentUser }: {} = initialState || {};
    const { accessKeySet = new Set() } = currentUser || {};

    if (accessKeySet.has(accessName)) {
      return React.createElement(Component, props);
    }

    if (type === 'disable') {
      return React.createElement(Component, {...props, disabled: true, onClick: undefined});
    }

    return null;
  };
}

export default withAccess;
