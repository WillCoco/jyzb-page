/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState: { currentUser?: API.CurrentUser | undefined }) {
  const { currentUser } = initialState || {};
  const { accessKeySet = new Set() } = currentUser || {};

  if (!accessKeySet) {
    return {};
  }

  return {
    "home": accessKeySet.has('home'),
    "project": accessKeySet.has('project'),
    "receive": accessKeySet.has('receive'),
    "plan": accessKeySet.has('plan'),
    "confirm": accessKeySet.has('confirm'),
    "cash": accessKeySet.has('cash'),
    "access": accessKeySet.has('user') || accessKeySet.has('role'),
    "user": accessKeySet.has('user'),
    "role": accessKeySet.has('role'),
    "bord": accessKeySet.has('bord'),
    "project_add": accessKeySet.has('project_add'),
    "project_edit": accessKeySet.has('project_edit'),
    "project_del": accessKeySet.has('project_del'),
    "confirmIncome_add": accessKeySet.has('confirmIncome_add'),
    "confirmIncome_edit": accessKeySet.has('confirmIncome_edit'),
    "confirmIncome_del": accessKeySet.has('confirmIncome_del'),
    "pay_add": accessKeySet.has('pay_add'),
    "pay_edit": accessKeySet.has('pay_edit'),
    "pay_del": accessKeySet.has('pay_del'),
    "plan_edit": accessKeySet.has('plan_edit'),
    "node_add": accessKeySet.has('node_add'),
    "node_edit": accessKeySet.has('node_edit'),
    "node_del": accessKeySet.has('node_del'),
    "record_add": accessKeySet.has('record_add'),
    "record_edit": accessKeySet.has('record_edit'),
    "record_del": accessKeySet.has('record_del'),
    "role_add": accessKeySet.has('role_add'),
    "role_edit": accessKeySet.has('role_edit'),
    "role_del": accessKeySet.has('role_del'),
    "tax_add": accessKeySet.has('tax_add'),
    "tax_edit": accessKeySet.has('tax_edit'),
    "tax_del": accessKeySet.has('tax_del'),
    "user_add": accessKeySet.has('user_add'),
    "user_edit": accessKeySet.has('user_edit'),
    "user_del": accessKeySet.has('user_del'),
    "user_reset": accessKeySet.has('user_reset'),
  };
}
