import { Request, Response } from 'express';

const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

async function getFakeCaptcha(req: Request, res: Response) {
  await waitTime(2000);
  return res.json('captcha-xxx');
}

const { ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION } = process.env;

/**
 * 当前用户的权限，如果为空代表没登录
 * current user access， if is '', user need login
 * 如果是 pro 的预览，默认是有权限的
 */
let access = ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site' ? 'admin' : '';

const getAccess = () => {
  return access;
};

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  // 支持值为 Object 和 Array
  'GET /api/currentUser': (req: Request, res: Response) => {
    if (!getAccess()) {
      res.status(401).send({
        data: {
          isLogin: false,
        },
        errorCode: '401',
        errorMessage: '请先登录！',
        success: true,
      });
      return;
    }
    res.send({
      success: true,
      data: {
        name: 'Serati Ma',
        avatar: 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
        userid: '00000001',
        email: 'antdesign@alipay.com',
        signature: '海纳百川，有容乃大',
        title: '交互专家',
        group: '蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED',
        tags: [
          {
            key: '0',
            label: '很有想法的',
          },
          {
            key: '1',
            label: '专注设计',
          },
          {
            key: '2',
            label: '辣~',
          },
          {
            key: '3',
            label: '大长腿',
          },
          {
            key: '4',
            label: '川妹子',
          },
          {
            key: '5',
            label: '海纳百川',
          },
        ],
        notifyCount: 12,
        unreadCount: 11,
        country: 'China',
        access: getAccess(),
        geographic: {
          province: {
            label: '浙江省',
            key: '330000',
          },
          city: {
            label: '杭州市',
            key: '330100',
          },
        },
        address: '西湖区工专路 77 号',
        phone: '0752-268888888',
      },
    });
  },
  // GET POST 可省略
  'GET /api/users': [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
    },
  ],
  'GET /devProxy/user/1': async (req: Request, res: Response) => {
    const { password, username, type } = req.body || {};
      await waitTime(2000);
      res.send({
        code: 200,
        type,
        data: {
          "id": 1,
          "realName": "管理员",
          "username": "admin",
          "btnAuthList": [
      {
        "id": 10,
        "key": "project_add",
        "name": "新增项目"
      },
      {
        "id": 11,
        "key": "project_edit",
        "name": "编辑项目"
      },
      {
        "id": 12,
        "key": "project_del",
        "name": "删除项目"
      },
      {
        "id": 13,
        "key": "confirmIncome_add",
        "name": "新增确认收入"
      },
      {
        "id": 14,
        "key": "confirmIncome_edit",
        "name": "修改确认收入"
      },
      {
        "id": 15,
        "key": "confirmIncome_del",
        "name": "删除收确认收入"
      },
      {
        "id": 16,
        "key": "pay_add",
        "name": "新增新增支出"
      },
      {
        "id": 17,
        "key": "pay_edit",
        "name": "修改确认收入"
      },
      {
        "id": 18,
        "key": "pay_del",
        "name": "删除收确认收入"
      },
      {
        "id": 19,
        "key": "plan_edit",
        "name": "修改收款计划"
      },
      {
        "id": 20,
        "key": "node_add",
        "name": "新增收款节点"
      },
      {
        "id": 21,
        "key": "node_edit",
        "name": "修改收款节点"
      },
      {
        "id": 22,
        "key": "node_del",
        "name": "删除收款节点"
      },
      {
        "id": 23,
        "key": "record_add",
        "name": "新增收款记录"
      },
      {
        "id": 24,
        "key": "record_edit",
        "name": "修改收款记录"
      },
      {
        "id": 25,
        "key": "record_del",
        "name": "删除收款记录"
      },
      {
        "id": 26,
        "key": "role_add",
        "name": "新增角色"
      },
      {
        "id": 27,
        "key": "role_edit",
        "name": "修改角色"
      },
      {
        "id": 28,
        "key": "role_del",
        "name": "删除角色"
      },
      {
        "id": 29,
        "key": "tax_add",
        "name": "新增开票"
      },
      {
        "id": 30,
        "key": "tax_edit",
        "name": "修改开票"
      },
      {
        "id": 31,
        "key": "tax_del",
        "name": "删除开票"
      },
      {
        "id": 32,
        "key": "user_add",
        "name": "新增用户"
      },
      {
        "id": 33,
        "key": "user_edit",
        "name": "修改开票"
      },
      {
        "id": 34,
        "key": "user_del",
        "name": "删除开票"
      },
      {
        "id": 35,
        "key": "user_reset",
        "name": "重置用户密码"
      }
    ],
    "menuAuthList": [
      // {
      //   "id": 1,
      //   "key": "home",
      //   "name": "首页"
      // },
      {
        "id": 2,
        "key": "project",
        "name": "项目管理"
      },
      {
        "id": 3,
        "key": "receive",
        "name": "收款管理"
      },
      {
        "id": 4,
        "key": "plan",
        "name": "收款计划"
      },
      {
        "id": 5,
        "key": "confirm",
        "name": "确认收入"
      },
      {
        "id": 6,
        "key": "cash",
        "name": "现金流"
      },
      {
        "id": 7,
        "key": "user",
        "name": "用户管理"
      },
      {
        "id": 8,
        "key": "role",
        "name": "角色管理"
      },
      {
        "id": 9,
        "key": "bord",
        "name": "看板"
      }
    ]
        }
      });
      access = 'admin';

    // res.send({
    //   status: 'error',
    //   type,
    //   currentAuthority: 'guest',
    // });
    // access = 'guest';
  },
  'POST /api/login/outLogin': (req: Request, res: Response) => {
    access = '';
    res.send({ data: {}, success: true });
  },
  'POST /api/register': (req: Request, res: Response) => {
    res.send({ status: 'ok', currentAuthority: 'user', success: true });
  },
  'GET /api/500': (req: Request, res: Response) => {
    res.status(500).send({
      timestamp: 1513932555104,
      status: 500,
      error: 'error',
      message: 'error',
      path: '/base/category/list',
    });
  },
  'GET /api/404': (req: Request, res: Response) => {
    res.status(404).send({
      timestamp: 1513932643431,
      status: 404,
      error: 'Not Found',
      message: 'No message available',
      path: '/base/category/list/2121212',
    });
  },
  'GET /api/403': (req: Request, res: Response) => {
    res.status(403).send({
      timestamp: 1513932555104,
      status: 403,
      error: 'Forbidden',
      message: 'Forbidden',
      path: '/base/category/list',
    });
  },
  'GET /api/401': (req: Request, res: Response) => {
    res.status(401).send({
      timestamp: 1513932555104,
      status: 401,
      error: 'Unauthorized',
      message: 'Unauthorized',
      path: '/base/category/list',
    });
  },
  'GET  /api/login/captcha': getFakeCaptcha,
};
