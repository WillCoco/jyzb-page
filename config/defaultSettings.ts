import { Settings as LayoutSettings } from '@ant-design/pro-layout';

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
} = {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'mix',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  headerTheme: 'light',
  colorWeak: false,
  title: '全链事业部经营性指标管理平台',
  pwa: false,
  logo: false,
  iconfontUrl: '',
};

export default Settings;
