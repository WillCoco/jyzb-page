﻿export default [
    {
      path: '/user',
      layout: false,
      routes: [
        {
          path: '/user/login',
          layout: false,
          name: 'login',
          component: './user/Login',
        },
        {
          path: '/user',
          redirect: '/user/login',
        },
        // {
        //   name: 'register-result',
        //   icon: 'smile',
        //   path: '/user/register-result',
        //   component: './user/register-result',
        // },
        // {
        //   name: 'register',
        //   icon: 'smile',
        //   path: '/user/register',
        //   component: './user/register',
        // },
        {
          component: '404',
        },
      ],
    },
    // 首页
    {
      path: '/home',
      name: 'home',
      icon: 'dashboard',
      access: 'home',
      component: './dashboard/analysis',
    },
    // 项目
    {
      path: '/project',
      name: 'project-manage',
      component: '@/layouts/index',
      icon: 'project',
      access: 'project',
      routes: [
        {
          path: '/project',
          redirect: '/project/list',
        },
        {
          name: 'project-list',
          icon: 'project',
          path: '/project/list',
          component: './project/list',
          hideInMenu: true,
        },
        {
          name: 'project-detail',
          icon: 'project',
          path: '/project/list/detail/:id',
          component: './project/detail',
          hideInMenu: true,
        },
        {
          name: 'project-add',
          icon: 'project',
          path: '/project/list/add',
          component: './project/edit',
          hideInMenu: true,
        },
        {
          name: 'project-edit',
          icon: 'project',
          path: '/project/list/edit/:id',
          component: './project/edit',
          hideInMenu: true,
        },
      ],
    },
    // 应收款
    {
      path: '/receivables',
      name: 'receivables',
      icon: 'fileExclamation',
      component: '@/layouts/index',
      access: 'receive',
      routes: [
        {
          path: '/receivables',
          redirect: '/receivables/list',
        },
        {
          name: 'receivables-list',
          icon: 'fileExclamation',
          path: '/receivables/list',
          component: './receivables/list',
        },
        // {
        //   name: 'receivables-detail',
        //   icon: 'fileExclamation',
        //   path: '/receivables/detail/:id',
        //   component: './receivables/detail',
        // },
        {
          name: 'receivables-plan',
          icon: 'fileExclamation',
          path: '/receivables/plan',
          component: './receivables/plan',
        },
      ],
    },
    // 确认收入
    {
      path: '/income',
      name: 'income',
      icon: 'dollar',
      component: '@/layouts/index',
      access: 'confirm',
      routes: [
        {
          path: '/income',
          redirect: '/income/list',
        },
        {
          name: 'income-list',
          icon: 'dollar',
          path: '/income/list',
          component: './income/list',
        },
        {
          name: 'income-detail',
          icon: 'dollar',
          path: '/income/detail/:id',
          component: './dashboard/analysis',
          hideInMenu: true,
        },
      ],
    },
    // 现金流
    {
      path: '/cash-flow',
      name: 'cash-flow',
      icon: 'moneyCollect',
      component: '@/layouts/index',
      access: 'cash',
      routes: [
        {
          path: '/cash-flow',
          redirect: '/cash-flow/list',
        },
        {
          name: 'cash-flow-list',
          icon: 'smile',
          path: '/cash-flow/list',
          component: './cashFlow/list',
        },
      ],
    },
    // // 看板
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   icon: 'barChart',
    //   component: '@/layouts/index',
    //   access: 'bord',
    //   routes: [
    //     {
    //       path: '/dashboard',
    //       redirect: '/dashboard/index',
    //     },
    //     {
    //       name: 'dashboard',
    //       icon: 'barChart',
    //       path: '/dashboard/index',
    //       component: './dashboard/analysis',
    //       hideInMenu: true,
    //     },
    //   ],
    // },
    // 权限管理
    {
      path: '/access',
      name: 'access-manage',
      icon: 'idcard',
      component: '@/layouts/index',
      access: 'access',
      routes: [
        {
          path: '/access',
          redirect: '/access/user',
        },
        {
          name: 'access-user',
          icon: 'team',
          path: '/access/user',
          access: 'user',
          component: './access/user',
        },
        {
          name: 'access-role',
          icon: 'userSwitch',
          path: '/access/role',
          access: 'role',
          component: './access/role',
        },
      ],
    },
    // 账户
    {
      path: '/account',
      name: 'account',
      icon: 'moneyCollect',
      component: '@/layouts/index',
      hideInMenu: true,
      routes: [
        {
          path: '/account',
          redirect: '/account/settings',
        },
        {
          name: 'settings',
          path: '/account/settings',
          component: './account/settings',
        },
      ],
    },
    {
      path: '/',
      redirect: '/home',
      exact: true
    },
    {
      component: '404',
    },
  ]
